package loggers

import (
	"io"
	"log"
	"os"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

var (
	//TimeFormat is the global log time format
	TimeFormat string = "2006-01-02T15:04:05.000"

	//logrus logger with JSON formatter
	jsonLogger *logrus.Logger

	//standard logger for general output
	general *log.Logger
)

//InitLoggers initializes loggers
func InitLoggers(lvl logrus.Level) {
	//json logger
	jsonLogger = logrus.New()
	jsonLogger.Formatter = &logrus.JSONFormatter{
		// disable, as we set our own
		DisableTimestamp: true,
	}
	//set level to see more/less logs
	jsonLogger.SetLevel(lvl)

	//general logger
	general = log.New(new(logger), "", log.Lmsgprefix)
}

//GetJSONLogger returns the json logger
func GetJSONLogger() *logrus.Logger {
	if jsonLogger == nil {
		InitLoggers(logrus.TraceLevel)
	}
	return jsonLogger
}

//GetLogger returns the general logger
func GetLogger() *log.Logger {
	if general == nil {
		InitLoggers(logrus.TraceLevel)
	}
	return general
}

//GetTimestamp returns a string representation of time.Now() using TimeFormat
func GetTimestamp() string {
	return time.Now().Format(TimeFormat)
}

type logger struct{}

func (l *logger) getTimestamp(timeFormat *string) string {
	var format string
	if timeFormat == nil || len(*timeFormat) == 0 {
		format = TimeFormat
	} else {
		format = *timeFormat
	}
	return time.Now().Format(format)
}

func (l *logger) Write(p []byte) (n int, err error) {
	timestamp := l.getTimestamp(nil)
	var sb strings.Builder
	sb.WriteString(timestamp)
	sb.WriteString(" >> ")
	sb.WriteString(string(p))
	return os.Stdout.WriteString(sb.String())
}

func (l *logger) WriteString(w io.Writer, s string) (n int, err error) {
	timestamp := l.getTimestamp(nil)
	var sb strings.Builder
	sb.WriteString(timestamp)
	sb.WriteString(" >> ")
	sb.WriteString(s)
	return io.WriteString(w, sb.String())
}
