package basic

import (
	"gofx/interfaces"
	"gofx/model"
	"gofx/strategy"
)

func ProcessOpenTradeSignals(maxTrades int, openTrades map[string]*model.SignalTrade, utilizationPerTrade float64, filteredSignals []*model.AutochartistSignal, client interfaces.HTTPClient) []strategy.Action {
	b := &Basic{}
	b.maxTrades = maxTrades
	b.utilizationPerTrade = utilizationPerTrade
	return b.processOpenTradeSignals(filteredSignals, openTrades, client)
}

var GetWeightedMeanQualityScore = getWeightedMeanQualityScore
