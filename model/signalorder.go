package model

import (
	"time"

	"gofx/model/instruments"
)

//SignalOrder represents an order (stop/limit) associated with a signal
type SignalOrder struct {
	BaseNoID
	OrderID         string                 `json:"id"`
	Signal          *AutochartistSignal    `json:"signal"`
	StrategyName    string                 `json:"strategyName"`
	Instrument      instruments.Instrument `json:"instrument"`
	Units           int                    `json:"units"`
	Price           float64                `json:"price"`
	TakePrice       *float64               `json:"takePrice,omitempty"`
	Deadline        *time.Time             `json:"deadline,omitempty"`
	DeadlineRFC1123 string                 `json:"-"`
	StopPrice       *float64               `json:"stopPrice,omitempty"`
	TSDistance      *float64               `json:"tsDistance,omitempty"`
	TradeID         *string                `json:"tradeID,omitempty"`
	FilledAt        *time.Time             `json:"filledAt,omitempty"`
}
