package model

import (
	"encoding/json"
	"time"

	"gofx/env"
)

//Report represents the model for daily/weekly reports
type Report struct {
	Base
	Data *ReportData
}

//ReportData represents the model for reports
type ReportData struct {
	Base
	Alias           string
	AccountID       string
	StrategyName    string
	WindowOpenAt    time.Time
	WindowCloseAt   time.Time
	CloseBalance    float64
	UnrealizedPL    float64
	PL              float64
	ActiveTrades    int
	WinningTrades   int
	LosingTrades    int
	AvgProfitPerWin float64
	AvgLossPerLoss  float64
	StrategyConfig  string
}

//SetStrategyConfig sets the StrategyConfig field from current env
func (d *ReportData) SetStrategyConfig() {
	configJSON, _ := json.Marshal(newStrategyConfig())
	d.StrategyConfig = string(configJSON)
}

//helper model for report data
type strategyConfig struct {
	StrategyMaxTrades              int      `json:"strategyMaxTrades"`
	StrategyMarginUtilPerTrade     float64  `json:"strategyMarginUtilPerTrade"`
	StrategyMinSignallProbability  float64  `json:"strategyMinSignallProbability"`
	StrategyMinTradeDurationHours  int      `json:"strategyMinTradeDurationHours"`
	StrategyMaxTradeDurationHours  int      `json:"strategyMaxTradeDurationHours"`
	StrategyMaxTradesPerSignal     int      `json:"strategyMaxTradesPerSignal"`
	StrategyInstrumentNames        []string `json:"strategyInstruments"`
	StrategyIntervalMinutes        int      `json:"strategyIntervalMinutes"`
	StrategyWindowEarningsGoal     float64  `json:"strategyWindowEarningsGoal"`
	StrategyTradeEarningsThreshold float64  `json:"strategyTradeEarningsThreshold"`
	TradeWindowOpenDay             string   `json:"tradeWindowOpenDay"`
	TradeWindowOpenHour            int      `json:"tradeWindowOpenHour"`
	TradeWindowOpenMinute          int      `json:"tradeWindowOpenMinute"`
	TradeWindowCloseDay            string   `json:"tradeWindowCloseDay"`
	TradeWindowCloseHour           int      `json:"tradeWindowCloseHour"`
	TradeWindowCloseMinute         int      `json:"tradeWindowCloseMinute"`
	TradeDayClosingHour            int      `json:"tradeDayClosingHour"`
	TradeDayClosingMinute          int      `json:"tradeDayClosingMinute"`
}

func newStrategyConfig() strategyConfig {
	config := strategyConfig{}
	config.StrategyMaxTrades = env.GetConfig().StrategyMaxTrades
	config.StrategyMarginUtilPerTrade = env.GetConfig().StrategyMarginUtilPerTrade
	config.StrategyMinSignallProbability = env.GetConfig().StrategyMinSignallProbability
	config.StrategyMinTradeDurationHours = env.GetConfig().StrategyMinTradeDurationHours
	config.StrategyMaxTradeDurationHours = env.GetConfig().StrategyMaxTradeDurationHours
	config.StrategyMaxTradesPerSignal = env.GetConfig().StrategyMaxTradesPerSignal
	config.StrategyIntervalMinutes = env.GetConfig().StrategyIntervalMinutes
	config.StrategyWindowEarningsGoal = env.GetConfig().StrategyWindowEarningsGoal
	config.StrategyTradeEarningsThreshold = env.GetConfig().StrategyTradeEarningsThreshold
	config.TradeWindowOpenHour = env.GetConfig().TradeWindowOpenHour
	config.TradeWindowOpenMinute = env.GetConfig().TradeWindowOpenMinute
	config.TradeWindowCloseHour = env.GetConfig().TradeWindowCloseHour
	config.TradeWindowCloseMinute = env.GetConfig().TradeWindowCloseMinute
	config.TradeDayClosingHour = env.GetConfig().TradeDayClosingHour
	config.TradeDayClosingMinute = env.GetConfig().TradeDayClosingMinute

	//non-direct field mappings
	config.StrategyInstrumentNames = make([]string, 0)
	for _, inst := range env.GetConfig().StrategyInstruments {
		config.StrategyInstrumentNames = append(config.StrategyInstrumentNames, inst.Name())
	}
	config.TradeWindowOpenDay = env.GetConfig().TradeWindowOpenDay.String()
	config.TradeWindowCloseDay = env.GetConfig().TradeWindowCloseDay.String()

	return config
}
