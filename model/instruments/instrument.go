package instruments

const (
	//EurUsd is the EUR/USD pair
	EurUsd Instrument = iota + 1

	//EurAud is the EUR/AUD pair
	EurAud

	//EurCad is the EUR/CAD pair
	EurCad

	//EurChf is the EUR/CHF pair
	EurChf

	//EurJpy is the EUR/JPY pair
	EurJpy

	//EurNzd is the EUR/NZD pair
	EurNzd

	//EurSgd is the EUR/SGD pair
	EurSgd

	//EurGbp is the EUR/GBP pair
	EurGbp

	//UsdCad is the USD/CAD pair
	UsdCad

	//UsdHkd is the USD/HKD pair
	UsdHkd

	//UsdChf is the USD/CHF pair
	UsdChf

	//UsdJpy is the USD/JPY pair
	UsdJpy

	//UsdSgd is the USD/SGD pair
	UsdSgd

	//AudUsd is the AUD/USD pair
	AudUsd

	//AudNzd is the AUD/NZD pair
	AudNzd

	//AudCad is the AUD/CAD pair
	AudCad

	//AudChf is the AUD/CHF pair
	AudChf

	//AudHkd is the AUD/HKD pair
	AudHkd

	//AudJpy is the AUD/JPY pair
	AudJpy

	//AudSgd is the AUD/SGD pair
	AudSgd

	//CadChf is the CAD/CHF pair
	CadChf

	//CadHkd is the CAD/HKD pair
	CadHkd

	//CadJpy is the CAD/JPY pair
	CadJpy

	//CadSgd is the CAD/SGD pair
	CadSgd

	//ChfHkd is the CHF/HKD pair
	ChfHkd

	//NzdCad is the NZD/CAD pair
	NzdCad

	//NzdChf is the NZD/CHF pair
	NzdChf

	//NzdHkd is the NZD/HKD pair
	NzdHkd

	//NzdJpy is the NZD/JPY pair
	NzdJpy

	//NzdSgd is the NZD/SGD pair
	NzdSgd

	//NzdUsd is the NZD/USD pair
	NzdUsd

	//GbpUsd is the GBP/USD pair
	GbpUsd

	//GbpAud is the GBP/AUD pair
	GbpAud

	//GbpCad is the GBP/CAD pair
	GbpCad

	//GbpChf is the GBP/CHF pair
	GbpChf

	//GbpHkd is the GBP/HKD pair
	GbpHkd

	//GbpJpy is the GBP/JPY pair
	GbpJpy

	//GbpNzd is the GBP/NZD pair
	GbpNzd

	//GbpSgd is the GBP/SGD pair
	GbpSgd

	//HkdJpy is the HKD/JPY pair
	HkdJpy

	//SgdChf is the SGD/CHF pair
	SgdChf

	//SgdHkd is the SGD/HKD pair
	SgdHkd

	//SgdJpy is the SGD/JPY pair
	SgdJpy
)

//Instrument represents a currency pair or other tradeable asset
type Instrument int

//Valid returns whether enum value is valid
func (i Instrument) Valid() bool {
	return i >= EurUsd && i <= SgdJpy
}

//Name returns the instrument name
func (i Instrument) Name() string {
	switch i {
	case EurUsd:
		return "EUR_USD"
	case EurAud:
		return "EUR_AUD"
	case EurCad:
		return "EUR_CAD"
	case EurChf:
		return "EUR_CHF"
	case EurJpy:
		return "EUR_JPY"
	case EurNzd:
		return "EUR_NZD"
	case EurSgd:
		return "EUR_SGD"
	case EurGbp:
		return "EUR_GBP"
	case UsdCad:
		return "USD_CAD"
	case UsdHkd:
		return "USD_HKD"
	case UsdChf:
		return "USD_CHF"
	case UsdJpy:
		return "USD_JPY"
	case UsdSgd:
		return "USD_SGD"
	case AudUsd:
		return "AUD_USD"
	case AudNzd:
		return "AUD_NZD"
	case AudCad:
		return "AUD_CAD"
	case AudChf:
		return "AUD_CHF"
	case AudHkd:
		return "AUD_HKD"
	case AudJpy:
		return "AUD_JPY"
	case AudSgd:
		return "AUD_SGD"
	case CadChf:
		return "CAD_CHF"
	case CadHkd:
		return "CAD_HKD"
	case CadJpy:
		return "CAD_JPY"
	case CadSgd:
		return "CAD_SGD"
	case ChfHkd:
		return "CHF_HKD"
	case NzdCad:
		return "NZD_CAD"
	case NzdChf:
		return "NZD_CHF"
	case NzdHkd:
		return "NZD_HKD"
	case NzdJpy:
		return "NZD_JPY"
	case NzdSgd:
		return "NZD_SGD"
	case NzdUsd:
		return "NZD_USD"
	case GbpUsd:
		return "GBP_USD"
	case GbpAud:
		return "GBP_AUD"
	case GbpCad:
		return "GBP_CAD"
	case GbpChf:
		return "GBP_CHF"
	case GbpHkd:
		return "GBP_HKD"
	case GbpJpy:
		return "GBP_JPY"
	case GbpNzd:
		return "GBP_NZD"
	case GbpSgd:
		return "GBP_SGD"
	case HkdJpy:
		return "HKD_JPY"
	case SgdChf:
		return "SGD_CHF"
	case SgdHkd:
		return "SGD_HKD"
	case SgdJpy:
		return "SGD_JPY"
	}
	return ""
}

//ParseInstrument converts string name of instrument its value
func ParseInstrument(name string) Instrument {
	switch name {
	case "EUR_USD":
		return EurUsd
	case "EUR_AUD":
		return EurAud
	case "EUR_CAD":
		return EurCad
	case "EUR_CHF":
		return EurChf
	case "EUR_JPY":
		return EurJpy
	case "EUR_NZD":
		return EurNzd
	case "EUR_SGD":
		return EurSgd
	case "EUR_GBP":
		return EurGbp
	case "USD_CAD":
		return UsdCad
	case "USD_HKD":
		return UsdHkd
	case "USD_CHF":
		return UsdChf
	case "USD_JPY":
		return UsdJpy
	case "USD_SGD":
		return UsdSgd
	case "AUD_USD":
		return AudUsd
	case "AUD_NZD":
		return AudNzd
	case "AUD_CAD":
		return AudCad
	case "AUD_CHF":
		return AudChf
	case "AUD_HKD":
		return AudHkd
	case "AUD_JPY":
		return AudJpy
	case "AUD_SGD":
		return AudSgd
	case "CAD_CHF":
		return CadChf
	case "CAD_HKD":
		return CadHkd
	case "CAD_JPY":
		return CadJpy
	case "CAD_SGD":
		return CadSgd
	case "CHF_HKD":
		return ChfHkd
	case "NZD_CAD":
		return NzdCad
	case "NZD_CHF":
		return NzdChf
	case "NZD_HKD":
		return NzdHkd
	case "NZD_JPY":
		return NzdJpy
	case "NZD_SGD":
		return NzdSgd
	case "NZD_USD":
		return NzdUsd
	case "GBP_USD":
		return GbpUsd
	case "GBP_AUD":
		return GbpAud
	case "GBP_CAD":
		return GbpCad
	case "GBP_CHF":
		return GbpChf
	case "GBP_HKD":
		return GbpHkd
	case "GBP_JPY":
		return GbpJpy
	case "GBP_NZD":
		return GbpNzd
	case "GBP_SGD":
		return GbpSgd
	case "HKD_JPY":
		return HkdJpy
	case "SGD_CHF":
		return SgdChf
	case "SGD_HKD":
		return SgdHkd
	case "SGD_JPY":
		return SgdJpy
	}
	return 0
}
