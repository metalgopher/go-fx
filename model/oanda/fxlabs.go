package oanda

//AutochartistSignal represents an autochartist signal
type AutochartistSignal struct {
	ID         int            `json:"id"`
	Type       string         `json:"type"`
	Instrument string         `json:"instrument"`
	Data       SignalData     `json:"data"`
	Meta       SignalMetaData `json:"meta"`
}

//SignalData struct
type SignalData struct {
	Prediction     *SignalPrediction `json:"prediction,omitempty"`
	Points         SignalPoints      `json:"points"`
	PatternEndTime int               `json:"patternendtime"`
	Price          float64           `json:"price,omitempty"`
}

//SignalPrediction struct
type SignalPrediction struct {
	PriceLow  float64 `json:"pricelow"`
	PriceHigh float64 `json:"pricehigh"`
	TimeFrom  int64   `json:"timefrom"`
	TimeTo    int64   `json:"timeto"`
	TimeBars  int     `json:"timebars,omitempty"`
}

//SignalPoints struct
type SignalPoints struct {
	Support    *SignalPointsCoordinates `json:"support,omitempty"`
	Resistance *SignalPointsCoordinates `json:"resistance,omitempty"`
	KeyTime    map[int]int64            `json:"keytime,omitempty"`
}

//SignalPointsCoordinates struct
type SignalPointsCoordinates struct {
	Y1 float64 `json:"y1"`
	Y0 float64 `json:"y0"`
	X1 int64   `json:"x1"`
	X0 int64   `json:"x0"`
}

//SignalMetaData struct
type SignalMetaData struct {
	Scores          *MetaScores   `json:"scores"`
	PatternType     string        `json:"patterntype,omitempty"`
	Pattern         string        `json:"pattern"`
	Probability     float64       `json:"probability"`
	Interval        int           `json:"interval"`
	Direction       int           `json:"direction"`
	Length          int           `json:"length"`
	HistoricalStats MetaHistStats `json:"historicalstats"`
	TrendType       string        `json:"trendtype,omitempty"`
	Completed       int           `json:"completed"`
}

//MetaScores struct
type MetaScores struct {
	Clarity      *int `json:"clarity,omitempty"`
	InitialTrend *int `json:"initialtrend,omitempty"`
	Breakout     *int `json:"breakout,omitempty"`
	Quality      *int `json:"quality,omitempty"`
	Uniformity   *int `json:"uniformity,omitempty"`
}

//MetaHistStats struct
type MetaHistStats struct {
	Symbol    MetaHistStatData `json:"symbol"`
	Pattern   MetaHistStatData `json:"pattern"`
	HourOfDay MetaHistStatData `json:"hourofday"`
}

//MetaHistStatData struct
type MetaHistStatData struct {
	Correct int     `json:"correct"`
	Percent float64 `json:"percent"`
	Total   int     `json:"total"`
}
