package reporting

import (
	"fmt"
	"gofx/model"
	"net/http"
	"strconv"
	"strings"
	"time"

	fxhttp "gofx/client/http"
	"gofx/client/util"
	"gofx/db"
	"gofx/email"
	"gofx/env"
	"gofx/interfaces"
	"gofx/loggers"
)

//SendDailySummaryReportEmail generates a report for the day, based on trade day closing hour and minute, and sends via email
func SendDailySummaryReportEmail(nextDaySpotTime time.Time, recEmail, fromEmail string, baseData email.BaseTemplateData, client interfaces.HTTPClient, templatePath *string) error {
	// -- determine report begin/end times --
	var beginTime, endTime time.Time

	//EndTime
	if env.GetConfig().TradeDayClosingHour <= nextDaySpotTime.Hour() {
		endTime = time.Date(nextDaySpotTime.Year(), nextDaySpotTime.Month(), nextDaySpotTime.Day(), env.GetConfig().TradeDayClosingHour, env.GetConfig().TradeDayClosingMinute, 0, 0,
			time.Local).Add(time.Second * -1)
	} else {
		endTime = time.Date(nextDaySpotTime.Year(), nextDaySpotTime.Month(), nextDaySpotTime.Day(), env.GetConfig().TradeDayClosingHour, env.GetConfig().TradeDayClosingMinute, 0, 0,
			time.Local).Add(time.Hour * -24).Add(time.Second * -1)
	}

	//BeginTime
	beginTime = endTime.Add(time.Hour * -24).Add(time.Second * 1)

	return sendSummaryReportEmail(beginTime, endTime, "Daily", recEmail, fromEmail, baseData, client, templatePath)
}

//SendWeekEndingDailySummaryReportEmail generates a report for the final day of the week, the remainder of prev day close and window close, and sends via email
func SendWeekEndingDailySummaryReportEmail(nextWeekSpotTime time.Time, recEmail, fromEmail string, baseData email.BaseTemplateData, client interfaces.HTTPClient, templatePath *string) error {
	// -- determine report begin/end times --
	var beginTime, endTime time.Time

	_, windowEnd, isWithinWindow := util.GetCurrentTradingWindow(nextWeekSpotTime)
	if isWithinWindow {
		endTime = windowEnd
	} else {
		//if after window, subtract a week to get previously closed week's times
		endTime = windowEnd.Add(time.Hour * 24 * -7)
	}

	//BeginTime
	if env.GetConfig().TradeDayClosingHour < nextWeekSpotTime.Hour() || env.GetConfig().TradeDayClosingHour == nextWeekSpotTime.Hour() && env.GetConfig().TradeDayClosingMinute < nextWeekSpotTime.Minute() {
		beginTime = time.Date(nextWeekSpotTime.Year(), nextWeekSpotTime.Month(), nextWeekSpotTime.Day(), env.GetConfig().TradeDayClosingHour, env.GetConfig().TradeDayClosingMinute, 0, 0,
			time.Local)
	} else {
		beginTime = time.Date(nextWeekSpotTime.Year(), nextWeekSpotTime.Month(), nextWeekSpotTime.Day(), env.GetConfig().TradeDayClosingHour, env.GetConfig().TradeDayClosingMinute, 0, 0,
			time.Local).Add(time.Hour * -24)
	}

	return sendSummaryReportEmail(beginTime, endTime, "Daily", recEmail, fromEmail, baseData, client, templatePath)
}

//SendWeeklySummaryReportEmail generates a report for the week based on trade window and sends via email
func SendWeeklySummaryReportEmail(nextWeekSpotTime time.Time, recEmail, fromEmail string, baseData email.BaseTemplateData, client interfaces.HTTPClient, templatePath *string) error {
	// -- determine report begin/end times --
	var beginTime, endTime time.Time

	windowStart, windowEnd, isWithinWindow := util.GetCurrentTradingWindow(nextWeekSpotTime)
	if isWithinWindow {
		beginTime = windowStart
		endTime = windowEnd
	} else {
		//if after window, subtract a week to get previously closed week's times
		beginTime = windowStart.Add(time.Hour * 24 * -7)
		endTime = windowEnd.Add(time.Hour * 24 * -7)
	}

	return sendSummaryReportEmail(beginTime, endTime, "Weekly", recEmail, fromEmail, baseData, client, templatePath)
}

func sendSummaryReportEmail(beginTime, endTime time.Time, period, recEmail, fromEmail string, baseData email.BaseTemplateData, client interfaces.HTTPClient, templatePath *string) error {
	//get account info
	acctRes, err := fxhttp.GetAccount(&http.Client{})
	if err != nil || acctRes == nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not send %s report", strings.ToLower(period))
		return err
	}

	//create db report
	reportData := &model.ReportData{}
	reportData.Alias = acctRes.Alias
	reportData.AccountID = acctRes.ID
	reportData.StrategyName = baseData.StrategyName
	reportData.WindowOpenAt = beginTime
	reportData.WindowCloseAt = endTime
	reportData.CloseBalance, _ = strconv.ParseFloat(acctRes.Balance, 64)

	// -- calculuate derived report data fields --

	//query db for signal trades over the period
	activeTrades, err := db.GetStrategyTradesByTimeRange(baseData.StrategyName, beginTime, endTime, nil)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not send %s report", strings.ToLower(period))
		return err
	}

	reportData.ActiveTrades = len(activeTrades)
	for tradeID, trade := range activeTrades {
		if trade.ProfitLoss != nil {
			if *trade.ProfitLoss >= 0 {
				reportData.WinningTrades++
				reportData.AvgProfitPerWin += *trade.ProfitLoss
			} else {
				reportData.LosingTrades++
				reportData.AvgLossPerLoss -= *trade.ProfitLoss
			}
			reportData.PL += *trade.ProfitLoss
		} else {
			//open trade, get info from Oanda
			openTradeRes, err := fxhttp.GetTradeByID(tradeID, &http.Client{})
			if err != nil {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not send %s report", strings.ToLower(period))
				return err
			}
			unrealizedPL, _ := strconv.ParseFloat(openTradeRes.Trade.UnrealizedPL, 64)
			reportData.UnrealizedPL += unrealizedPL
		}
	}

	//calculate averages
	if reportData.WinningTrades > 0 {
		reportData.AvgProfitPerWin /= float64(reportData.WinningTrades)
	} else {
		reportData.AvgProfitPerWin = 0.
	}
	if reportData.LosingTrades > 0 {
		reportData.AvgLossPerLoss /= float64(reportData.LosingTrades)
	} else {
		reportData.AvgLossPerLoss = 0.
	}

	//write report to db
	var reportErr error
	var reportID int
	switch period {
	case "Daily":
		reportID, reportErr = db.CreateDailyReport(reportData, nil)
	case "Weekly":
		reportID, reportErr = db.CreateWeeklyReport(reportData, nil)
	default:
		reportErr = fmt.Errorf("invalid period: %s", period)
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(reportErr).Error("invalid report period received")
	}
	ts := loggers.GetTimestamp()
	if reportErr == nil {
		loggers.GetJSONLogger().WithField("ts", ts).WithField("reportID", reportID).Infof("%s report added to db successfully!", period)
	} else {
		loggers.GetJSONLogger().WithField("ts", ts).WithError(reportErr).Errorf("failed to write %s report to db", period)
	}

	// -- prepare report email template data --
	data := email.ReportTemplateData{}

	//base template data
	data.AppName = baseData.AppName
	data.CompanyName = baseData.CompanyName
	data.CopyrightYear = baseData.CopyrightYear
	data.CompanyLink = baseData.CompanyLink
	data.StrategyName = baseData.StrategyName

	//account data
	acctData := email.NewAccountTemplateData(acctRes)
	data.AccountTemplateData = *acctData

	//remaining report template data
	data.Period = period
	data.BeginTimeRFC1123 = beginTime.Format(time.RFC1123)
	data.EndTimeRFC1123 = endTime.Format(time.RFC1123)
	data.PL = fmt.Sprintf("%.3f", reportData.PL)
	data.UnrealizedPL = fmt.Sprintf("%.3f", reportData.UnrealizedPL)
	data.ActiveTrades = reportData.ActiveTrades
	data.WinningTrades = reportData.WinningTrades
	data.LosingTrades = reportData.LosingTrades
	data.ClosedTrades = reportData.WinningTrades + reportData.LosingTrades
	if reportData.WinningTrades+reportData.LosingTrades > 0 {
		data.RatioWinLoss = fmt.Sprintf("%.2f%%", 100*float64(reportData.WinningTrades)/float64(reportData.WinningTrades+reportData.LosingTrades))
	}
	if reportData.WinningTrades > 0 {
		data.ProfitPerWin = fmt.Sprintf("%.3f", reportData.AvgProfitPerWin)
		if reportData.LosingTrades > 0 {
			data.RatioPPWLPL = fmt.Sprintf("%.2f%%", 100*reportData.AvgProfitPerWin/reportData.AvgLossPerLoss)
		}
	}
	if reportData.LosingTrades > 0 {
		data.LossPerLoss = fmt.Sprintf("%.3f", reportData.AvgLossPerLoss)
	}
	data.Trades = activeTrades

	resCode, err := email.ActiveProvider.SendTXReport(recEmail, fromEmail, fmt.Sprintf("%s Summary Report", data.Period), data, client, templatePath)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithField("resCode", resCode).WithError(err).Errorf("could not send %s report", strings.ToLower(period))
	}

	//success
	ts = loggers.GetTimestamp()
	loggers.GetJSONLogger().WithField("ts", ts).Infof("%s report sent successfully!", period)
	return nil
}
