package cli

import (
	"fmt"
	"os"
	"strings"

	"gofx/client"
)

//Start begins the cli command interpretation
func Start() {
	args := os.Args
	if len(args) < 2 {
		showUsage()
		os.Exit(1)
	}

	switch args[1] {
	case "start":
		tradeClient()
	default:
		showUsage()
	}
}

func tradeClient() {
	client.DisplayTitle()
	client.StartupTasks()
	client.Run()
}

func backTest() {
	//TODO
}

func showUsage() {
	fmt.Println(strings.Replace(usageMsg, "\n", "", 1))
}

var usageMsg = `
usage: gofx [start]

Here are the main commands:

start the trading client/application
    start
`
