![Go FX](./img/gofx_header.svg)

[![pipeline status](https://gitlab.com/go-fx/go-fx/badges/master/pipeline.svg)](https://gitlab.com/go-fx/go-fx/-/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/go-fx/go-fx)](https://goreportcard.com/report/gitlab.com/go-fx/go-fx)
[![coverage report](https://gitlab.com/go-fx/go-fx/badges/master/coverage.svg)](https://gitlab.com/go-fx/go-fx/-/jobs/artifacts/master/download?job=code_coverage_publish)
[![License](https://img.shields.io/badge/license-Apache%202-blue)](LICENSE)

<br/>

## Overview

- `Go FX` is a [Forex](https://www1.oanda.com/forex-trading/learn/getting-started/first-trade) trading application that trades on the Oanda Forex Broker.  You can read more about Oanda [here](https://www.oanda.com/group/about-us/).  
- `Go FX` fetches signals from the Oanda trading signal service called [Autochartist](https://developer.oanda.com/rest-live/forex-labs/) to make trading easy and fun!
- `Go FX` is free, open-source software that can be ran with a free Oanda broker account, free SendGrid account, and free MySQL DB (w/AWS Free Tier account).  Full setup guides are provided in this README.
- `Go FX` uses `SendGrid` (free account) to send email alerts for trade opening/closing, and summary reports (daily/weekly).  You can also track your trades with the Oanda web/desktop/mobile apps!
- `Go FX` connects with a MySQL database (free w/AWS Free Tier account) to track the progress of your active strategy, keeping it synchronized with the broker
- Users can run the included `Basic` Strategy or easily create new strategies too!
- `Go FX` is highly configurable.  Choose your trading window, strategy action interval, minimum signal probability, which instruments you want to trade, and more.
- Strategy actions can include opening a new market trade, placing market orders for future fulfillment, closing trades, modifying trades, or just observing the market.
- `Go FX` runs a single active strategy at a time but you can run multiple instances of the application at once, each with a different configuration and strategy, while sharing the same DB, SendGrid, and Oanda accounts.
- Oanda features an excellent API for trading on Forex programmatically, called the [v20 REST API](https://developer.oanda.com/rest-live-v20/introduction/).  `Go FX` uses this `v20` API to integrate with the Oanda broker.
- You can open a free Oanda account and use their `v20` API on a free practice account to simulate real trading for no up-front cost!
- Track your trades in real time on the Oanda Trading [Desktop](https://www.oanda.com/us-en/trading/platforms/oanda-trade-desktop/), [Web Application](https://trade.oanda.com/) or [Mobile Apps](https://www.oanda.com/us-en/trading/platforms/oanda-trade-mobile/)!
- Beginners can start without Git or source code! Just go through the following 3 parts of this README:
  1. [Initial Setup Guide](#initial-setup-guide)
  2. [Environment File Setup](#environment-file-setup)
  3. [Running Latest Release](#running-latest-release)

<br/>

![Go FX Preview](./img/preview_screencap.gif)

<br/>

<h2>Table of Contents</h2>

- [Initial Setup Guide](#initial-setup-guide)
  - [Oanda v20 Account](#oanda-v20-account)
  - [SendGrid Account](#sendgrid-account)
  - [MySQL Database](#mysql-database)
    - [MySQL on AWS](#mysql-on-aws)
  - [Developer Setup](#developer-setup)
- [Environment File Setup](#environment-file-setup)
  - [Variable Definitions](#variable-definitions)
    - [**OS**](#os)
    - [**App**](#app)
    - [**DB**](#db)
    - [**Strategy**](#strategy)
    - [**Trading**](#trading)
    - [**Alerts**](#alerts)
    - [**Provider**](#provider)
- [Running Latest Release](#running-latest-release)
- [Local Development](#local-development)
  - [Run Local Build](#run-local-build)
  - [Custom Releases](#custom-releases)
  - [Testing](#testing)
    - [Go Test](#go-test)
    - [Custom Testing Tool](#custom-testing-tool)
- [Contributing](#contributing)
- [Authors](#authors)
- [License](#license)
- [Useful Links](#useful-links)
  - [Oanda v20 REST API Reference](#oanda-v20-rest-api-reference)
  - [Oanda Forex Labs](#oanda-forex-labs)
  - [Buyable Units Calculator](#buyable-units-calculator)
  - [Oanda Hours of Operation](#oanda-hours-of-operation)

<br/>

# Initial Setup Guide

To run `Go FX`, you'll have to set up the necessary prerequisite services.  This is a short guide which will take you through the main steps of that process.

<br/>

## Oanda v20 Account
First and foremost, you'll need to setup your free Oanda v20 trading account.  It's a good idea to begin with a practice account, which you can sign up for [here](https://fxtrade.oanda.com/your_account/fxtrade/register/gate).

[![Oanda Sign Up](./img/oanda_signup.png)](https://fxtrade.oanda.com/your_account/fxtrade/register/gate)

Once you have your account setup, visit your account [Manage API Access](https://fxtrade.oanda.com/demo-account/tpa/personal_token) page to retrieve your authorization token.

**Save your access token** for the [Environment File Setup](#environment-file-setup) later on and continue to the next prerequisite...

<br/>

## SendGrid Account
SendGrid is a Twilio company that functions as an API based messaging service from which you can send individual transactional emails or email marketing campaigns.  You can sign up for a free account, granting you up to 100 free transactional emails per day, plenty for `Go FX`.  You can [sign up for your free account here](https://signup.sendgrid.com/).

[![SendGrid Sign Up](./img/sendgrid_signup.png)](https://signup.sendgrid.com/)  

Once signed up, you can create/retrieve your API Key from the [API Keys page](https://app.sendgrid.com/settings/api_keys) within your account settings.

**Save your API key** for the [Environment File Setup](#environment-file-setup) later on and continue to the next prerequisite...

<br/>
  
## MySQL Database
Having a MySQL database is required to run `Go FX` as it keeps your trade, signal, and order information in a persistent state for ongoing execution.  One option is using an AWS Free Tier account and their *RDS* service.  If you're interested in that option, a detailed walkthrough guide is provided in the next section: [MySQL on AWS](#mysql-on-aws).  

There are several options for gaining access to a MySQL database since it is open source technology.  Whichever option you choose, it is first recommended to install [MySQL Workbench](https://dev.mysql.com/downloads/workbench/) - also open source - for easy interaction with your data from a graphical desktop application.

Upon starting workbench, you usually need 4 pieces of data to connect to a database:
1. The database host address (just `localhost` if running a DB locally)
2. The port (usually 3306)
3. Database username
4. Password for username
5. Schema/Database name

There are some great [resources online](https://www.digitalocean.com/community/tutorials/how-to-connect-to-a-mysql-server-remotely-with-mysql-workbench) to help you connect to remote databases.  Some methods of running a MySQL server include:
- Running via a major cloud services provider
- Running via a niche cloud MySQL provider
- Running locally via locally installed [MySQL Server](https://dev.mysql.com/downloads/mysql/)
- Running locally via a [Docker container](https://hub.docker.com/_/mysql)

**The next section of this guide will focus on setting up a MySQL server instance on a major cloud services provider.  Feel free to skip it if you already have your DB connected.**

<br/>

### MySQL on AWS
**Amazon Web Services (AWS)** is one of the largest cloud services providers around, if not THE largest.  AWS offers a [free tier account](https://aws.amazon.com/free), which allows you to utilize many AWS services.  With a free tier account, you can create a free small MySQL instance hosted in the cloud.  

This section is a walkthough of how to automatically setup MySQL on AWS via **CloudFormation**.  CloudFormation is a service that allows you to automatically create sets of AWS resources - called `stacks` - and manage those stacks as individual units.  For convenience, an AWS [CloudFormation template](./infra/aws/cloudformation/mysql-db.yml) for creating a free tier MySQL instance has been provided in this project.  A direct download link for the template is provided in step 3 below.  

First, you'll need to sign up for your AWS Free Tier account:

[![AWS Free Tier Sign Up](./img/aws_free_tier_signup.png)](https://aws.amazon.com/free)

Once you are logged in with your free tier AWS account, you can create a MySQL database with the following steps:
1. Navigate to the [AWS CloudFormation](https://console.aws.amazon.com/cloudformation/home) service
2. On the right side, expand the **`Create Stack`** menu and choose **`With new resources (standard)`**

![Create Stack](./img/aws_cf_create_stack.png)

3. Download the [CloudFormation template](https://gitlab.com/go-fx/go-fx/-/raw/master/infra/aws/cloudformation/mysql-db.yml?inline=false) that is included with this project and save it somewhere.
4. Under `Specify Template`, select the **`Upload a template file`** radio button and click the **`Choose file`** button to upload the template file you downloaded in the previous step.

![Upload CF Template](./img/aws_cf_upload_template.png)

5. Click **`Next`** and on the next screen, enter a **stack name**.  The **stack name** will be the name of your DB resource in AWS but does not have to match the actual name of the DB/schema you choose.

6. Next, fill out the 3 parameters on this same screen: `DBName`, `DBPassword`, & `DBUser` (Note: the `DBUser` & `DBPassword` you are specifying is for the DB admin user)

![CF Stack Params](./img/aws_cf_create_stack_db_params.png)

YOU DO NOT NEED TO CHANGE ANY OTHER PARAMETERS.  When ready, hit **`Next`** to proceed.

7. Hit **`Next`** again on the following screen.  That should take you to `Step 4: Review`, where you just need to scroll to the bottom and hit **`Create Stack`** to create this stack of resources.
8. At this point, you'll have to wait until the DB/stack is fully created which could take a few minutes.  You can refresh the stack status on the left periodically if you like:

![CF Stack In Progress](./img/aws_cf_create_stack_in_prog.png)

9. When the stack has finished being created, click the **`Outputs`** tab where you will find the values you need to connect to your new DB.  

![MySQLConnection](./img/aws_cf_stack_outputs.png)

10. **Save the 5 values that begin with 'DB'** for the [Environment File Setup](#environment-file-setup) later on and continue
11. (Optional) Note: You may not want to leave your password exposed in this `Output` tab.  To hide these values, take the following steps:  
    a. Modify the `mysql-db.yml` file by putting a `#` sign at the start of the `Value: !Ref 'DBPassword'` line towards the bottom of the file under `DBPassword`  
    b. Then, put a `#` sign at the start of the `Value: !Join ['', ['mysql://', !Ref 'DBUser', ':', !Ref 'DBPassword', ...` line a few lines below that under `MySQLConnection`  
    c. What you have done is commented out those 2 lines.  Now uncomment the line directly under each of those respectively by removing the `#` signs that start each line: `# Value: 'PASSWORD_HIDDEN'` and `# Value: !Join ['', ['mysql://', !Ref 'DBUser', ':', 'PASSWORD_HIDDEN'` respectively  
    d. Now, we just need to update the stack with this updated template.  Only the `Outputs` will be affected.  Once you've saved the changes to `mysql-db.yml`, click **`Update`** in your CloudFormation Stacks view.  
    e. Choose **`Replace current template`** at the top, **`Upload a template file`** below and then **`Choose file`** where you'll select the updated template file `mysql-db.yml`  
    f. Then, select **`Next`** at the bottom, then **`Next`** again at the bottom of the next 2 screens, and finally **`Update stack`** at the bottom of the final screen.  
    g. You'll be taken back to your stacks view, click on the **`Outputs`** tab again and you should see your password hidden in both the `DBPassword` and `MySQLConnection` fields.  
    h. Note: You can always reverse the commented lines in the template file back to their original state and update the stack again to view your password again  

12. Now connect to your new DB with MySQL Workbench.  Open Workbench and choose the plus symbol to create a new connection:  

![MySQL Workbench Create Connection](img/mysql_wb_create_conn.png)

13. Enter the 5 values from the `Output` tab you just copied from CloudFormation:

![MySQL New Connection](img/mysql_wb_new_conn.png)

- `Connection Name` at the top is whatever you want to see when you open Workbench, that's all.
- `Hostname` is the **DBHost** value from CloudFormation.
- `Port` is **DBPort**.
- `Username` is **DBUsername**.
- `Password` is **DBPassword**.
- `Default Schema` is **DBName**.

Choose **`Test Connection`** to ensure success:

![MySQL WB Test Conn](img/mysql_wb_test_conn.png)

Hit **`OK`** to finish adding the new connection.

14. Click on the new connection you just made to connect to the DB (enter password if prompted).  You should now be looking at a blank .sql file where you can run queries.  We are now going to fix the timezone issue that comes with this new DB instance.
15. Enter the query `SELECT current_timestamp();` in the editor and hit the **lighting bolt** icon above to run it.  You should see output like this:

![MySQL WB TS](img/mysql_wb_get_ts.png)

This time is in UTC and likely will not match your local time.  Let's fix that.

16. Head back to CloudFormation and select the **`Resources`** tab, just left of the `Outputs` tab within the same stacks view.  Click on the **name of your stack** under the `Physical ID` column, which should be a link that will open the DB config screen in a new tab/window.

![CF Open DB Res](img/aws_cf_open_db_resource.png)

This is the home for your DB instance.  Explore if you like.  For now, we need to create our own custom parameter group to edit the timezone since we can't edit the default parameter group.

17. Choose **`Parameter Groups`** in the left side menu
18. Click on **`Create parameter group`** at the top right
19. Enter any `Group name` and `Description` (both required) and select **`Create`**.  If you're not feeling terribly creative, just use `CustomGroup` and `Custom group` respectively to save some brain cycles 😁
20. Now you're taken back to the list of groups.  Click on the **name of the group** you just made.  Then, click **`Edit parameters`** at the top right.
21. In the search field towards the top left, enter `time_zone`.  You'll see the parameter of the same name come up with a drop-down menu.  Choose the time zone based on the time zone where Go FX will be configured.  For most cases, this will be your local time zone.  For the US, there are some general time zones at the bottom of the drop-down list.  For a complete reference, check out [this list](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones).

![AWS RDS Params TZ](img/aws_rds_params_tz.png)

Now click **`Save changes`** in the upper right corner.  This updated the time zone in this new parameter group but now you must change the DB parameter group to this new one and apply the changes.

22. Click **`Databases`** towards the top of the left side menu and click on your DB/resource name to get back to its home screen.  Click **`Modify`** at the top right.
23. Scroll to `Database Options` and edit the **`DB parameter group`** drop-down option to the new group you just created and edited.

![AWS RDS Modify Param Group](img/aws_rds_modify_param_group.png)

Do not modify any other options.  Just click **`Continue`** at the bottom of the page.

24. Under `Scheduling of modifications`, select **`Apply immediately`** and then **`Modify DB instance`** at the bottom.  This will bring you back to your DB home view but you must refresh the page to see the `Info` towards the top update to `Modifying`.
25. Refresh every 15-30 seconds until `Info` goes back to `Available`.  Once it does, your parameter group has been updated.  However, you need to reboot the DB to apply the changes.
26. Reboot the DB instance by expanding the **`Actions`** drop-down menu towards the top right and selecting **`Reboot`**.  Select **`Reboot`** again on the next screen.  As before, you'll see the `Info` field update but this time it will say `Rebooting`.  Refresh every 15-30 seconds until it says `Available` again.  That should complete your time zone update.  Let's test in MySQL Workbench again.

27. In MySQL Workbench, hit the lightning bolt to run the above query again: `SELECT current_timestamp();`.  If it shows your local date and time (24-hour format), congrats!  **You're DB is ready!**

<br/>

**With your DB configured, you are done with the prerequisite setup required to run Go FX and are ready to proceed to the [Environment File Setup](#environment-file-setup).**  

<br/>

## Developer Setup
If you would like to work on the `Go FX` code to make changes, create your own strategies, etc., then you'll need to install 3 additional tools (Git, Go, & Docker):
- You can [install Git here](https://git-scm.com/downloads).
- You can [install Go here](https://golang.org/dl/).  
- You can [install Docker here](https://docs.docker.com/get-docker/).  

<br/>

**You are now done with the prerequisite setup required to run and/or modify Go FX and are ready to proceed to the [Environment File Setup](#environment-file-setup).**

<br/>

# Environment File Setup

`Go-FX` offers a high level of configuration, allowing you to fine-tune your strategy without frequent code changes.  This section covers each part of that configuration in detail.  

There are quite a few environment variables you can set for the `Go FX` application.  This section will describe how each of these variables affects the active strategy (i.e. **Basic** strategy which is included in this project).  

- The first step in creating a new environment for `Go FX` is to make a local copy of the included [.env](env/.env) file within the same `env/` directory.  
  - Any `.env` file will be ignored via the `.gitignore` file except for the original **.env** file itself.  So, choosing any other name will be safe as it will be ignored by Git.  
  - The name `dev.env` is recommended as this is the default file used by the [build_run_local.sh](tools/docker/build_run_local.sh) file (see Local Development section for more info).

<br/>

## Variable Definitions
### **OS**
- **TZ**
  
The `TZ` value is the name of the timezone to be used as the local application time.  A complete list of supported values can be found [here](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones).

This value is used to directly set the timezone of the `Go FX` Docker release image.  It will, therefore, also be the [Location](https://golang.org/pkg/time/#Location) returned by [time.Local()](https://golang.org/pkg/time/#Time.Localq) within the Go source code.  
- Notes:  
  1. Time zone locations are more accurate than a UTC offset since, for example, Daylight Savings Time can cause different offsets throughout the year.
  2. This value also sets the local time zone for database time values generated by `Go FX`.  However, you will still need to ensure the database shares this same value on its own for accurate DB generated times such as `current_timestamp()`.

<br/>

### **App**
- **APP_HOST_PROTOCOL**  

The `APP_HOST_PROTOCOL` is the HTTP protocol (`http`/`https`)of the host at which the app is running.  It is currently not used but can be used to create an API with client interactions so that the app knows its own host protocol.  

- **APP_HOST_NAME**  
  
The `APP_HOST_NAME` is the full host name at which the app is running.  It is currently not used but can be used to create an API with client interactions so that the app knows its own host name.  

- **APP_NAME**  

The `APP_NAME` is the client-centric name of the application.  It is currently used in email notification templates.  This value is optional.  

- **APP_COMPANY**  

The `APP_COMPANY` is the client-centric name of the company behind the application.  It is currently used in email notification templates.  This value is optional.  

- **APP_COMPANY_LINK**  

The `APP_COMPANY_LINK` is the URL of the home page for the application.  It is currently used in email notification templates.  This value is optional.  

- **SERVER_ENV**  

The `SERVER_ENV` is the name of the `Env` enumeration value that specifies the application environment.  You can find the names in the [env.go](./env/env.go#L21) file.  Current valid values are `DEV`, `STAGE`, & `PROD`:
  - `DEV` environment does not normalize the trading window to live market hours.  This is for testing purposes.  In other words, if you want to test after hours and/or with odd trading windows, this is only possible in the `DEV` environment.
  - `STAGE` environment normalizes the trading window to live market hours, which is Sunday 5pm to Friday 5pm, New York, NY USA local time.
  - `PROD` environment normalizes the trading window to live market hours, which is Sunday 5pm to Friday 5pm, New York, NY USA local time.  In the future this environment may also perform additional functions, such as persistence of real-time logs and/or signals.

- **SERVER_PORT**  

The `SERVER_PORT` is the port on which the server may be listening, possibly for a client API.  It is currently not used.  

- **SERVER_LOG_LEVEL**  

The `SERVER_LOG_LEVEL` is the log level for the application JSON logger.  Possible values consist of supported log levels from the [logrus](https://github.com/sirupsen/logrus) package.  `TRACE` is the lowest value which will cause all possible messages to be logged.  `INFO` is the usual choice as it logs pertinent information that may be helpful to see.  Higher log levels generally only log messages if things go wrong.  See possible values [here](./env/env.go#L155).  This value is required.  

<br/>

### **DB**
- **DB_HOST**  

The `DB_HOST` is the full address of the MySQL DB host for the application.  This value should not include the trailing colon `:` or the port.  This is required.  

- **DB_PORT**  

The `DB_PORT` is the numeric port for your MySQL DB host, usually `3306`.  Don't include the colon `:`.  This is required.  

- **DB_USERNAME**  

The `DB_USERNAME` is the username to log into the MySQL DB.  This is required.  

- **DB_PASSWORD**  

The `DB_PASSWORD` is the password to log into the MySQL DB with the `DB_USERNAME`.  This is required.  

- **DB_SCHEMA**  

The `DB_SCHEMA` is the name of the actual database within the MySQL connection.  If this db/schema does not exist on app startup, it will be created along with the required tables.  This is required.  

- **DB_SCRIPTS**  

The `DB_SCRIPTS` value is a comma-separated list of db scripts to execute on startup.  These should be names of `.sql` files within the `db/sql` directory.  This value is optional.  

<br/>

### **Strategy**
- **STRATEGY_NAME**  

The `STRATEGY_NAME` is the name of the active strategy to use for client execution.  If not provided or invalid, the [Basic](./strategy/basic/basic.go) strategy will be used.  Since only the Basic strategy is included with the project, this value is optional.

- **STRATEGY_MAX_TRADES**  

The `STRATEGY_MAX_TRADES` value is an integer representing the number of active trades the strategy should manage at once.  A higher value means the strategy can potentially open more trades, using more margin.  This should be calibrated along with other strategy values.  This is require.  

- **STRATEGY_MARGIN_UTIL_PER_TRADE**  

The `STRATEGY_MARGIN_UTIL_PER_TRADE` is a decimal number between 0 and 1.  It represents the percentage of available account margin which should be utilized per open trade.  Keep in mind the total margin utilized will be multiplied by up to the max number of trades set by the `STRATEGY_MAX_TRADES` value.  This is required.  

- **STRATEGY_MIN_SIGNAL_PROBABILITY**  

The `STRATEGY_MIN_SIGNAL_PROBABILITY` is the minimum overall probability which an Autochartist signal must meet for further consideration.  [Autochartist signals](./model/oanda/fxlabs.go) have other historical probabilities as well but this refers to the overall probability that the signal will be accurate.  Most good signals have a probability of 60-80%.  This is required.  

- **STRATEGY_INSTRUMENTS**  

The `STRATEGY_INSTRUMENTS` value is a comma-separated list of instrument names.  Supported instrument names can be found in the [instruments](./model/instruments/instrument.go#L47) package.  Only signals involving instruments in this list will be considered by Basic strategy so include more instruments here to cast a wider net.

- **STRATEGY_INTERVAL_MINUTES**  

The `STRATEGY_INTERVAL_MINUTES` value is an integer value representing the number of minutes to wait between strategy intervals.  Strategies are "ran" once per interval.  So, if you put 60 for this, your strategy will run once per hour.  Autochartist signals are updated no less than every 15 minutes.  Usually 15-240 minutes is a good range for this value.  It is also useful to set this to 1 minute with the server environment set to `DEV` for faster live testing.  This is required.  

- **STRATEGY_WINDOW_EARNINGS_GOAL**  

The `STRATEGY_WINDOW_EARNINGS_GOAL` value is a decimal value of at least 0.  The value represents the desired percentage increase of the starting account balance over the span of the current trading window.  For example, a value of `0.2` means your goal is a 20% increase over the starting account balance for the current trading window.  Therefore, only trades which meet or exceed that goal on an hourly basis will be considered.  For example, if your trading window is 4 days and a trade offers potential gains of 10% over 2 days, it would just meet the earnings goal but less gains in the same time would cause the Basic strategy to opt out as the earnings are too low.
  - Notes:
    - Higher margin util per trade makes the earnings goal easier to achieve but also comes with more risk.  Calibrate accordingly.  
    - Setting a higher/lower earnings goal only affects how picky the Basic strategy is.  Higher goal means more picky with signals.  Lower goal means less picky.  This goal value does not necessarily correlate with actual earnings.  It merely offers another means of filtering out signals for consideration.  

<br/>

### **Trading**
- **TRADE_WINDOW_OPEN_DAY**  

The `TRADE_WINDOW_OPEN_DAY` value is an integer value from 0 to 6, representing Sunday to Saturday.  The value specifies which day the trading WINDOW will open.  The trading window is a weekly period of time in which strategy execution may occur.  This value is subject to normalization in `STAGE` and `PROD` server environments.  This is required.  

- **TRADE_WINDOW_OPEN_HOUR**  

The `TRADE_WINDOW_OPEN_HOUR` value is an integer value from 0 to 23, representing an hour on a 24-hour scale.  The value specifies which hour the trading WINDOW will open.  The trading window is a weekly period of time in which strategy execution may occur.  This value is subject to normalization in `STAGE` and `PROD` server environments.  This is required.  

- **TRADE_WINDOW_OPEN_MINUTE**  

The `TRADE_WINDOW_OPEN_MINUTE` value is an integer value from 0 to 59, representing a minute on a 60-minute scale.  The value specifies which minute the trading WINDOW will open.  The trading window is a weekly period of time in which strategy execution may occur.  This is required.  

- **TRADE_WINDOW_CLOSE_DAY**  

The `TRADE_WINDOW_CLOSE_DAY` value is an integer value from 0 to 6, representing Sunday to Saturday.  The value specifies which day the trading WINDOW will close.  The trading window is a weekly period of time in which strategy execution may occur.  This value is subject to normalization in `STAGE` and `PROD` server environments.  This is required.  

- **TRADE_WINDOW_CLOSE_HOUR**  

The `TRADE_WINDOW_CLOSE_HOUR` value is an integer value from 0 to 23, representing an hour on a 24-hour scale.  The value specifies which hour the trading WINDOW will close.  The trading window is a weekly period of time in which strategy execution may occur.  This value is subject to normalization in `STAGE` and `PROD` server environments.  This is required.  

- **TRADE_WINDOW_CLOSE_MINUTE**  

The `TRADE_WINDOW_CLOSE_MINUTE` value is an integer value from 0 to 59, representing a minute on a 60-minute scale.  The value specifies which minute the trading WINDOW will close.  The trading window is a weekly period of time in which strategy execution may occur.  This is required.  

- **TRADE_DAY_CLOSING_HOUR**  
The `TRADE_DAY_CLOSING_HOUR` value is an integer value from 0 to 23, representing an hour on a 24-hour scale.  The value specifies which hour the trading DAY will close.  Trade day closing is provided for reporting purposes only as a way of breaking up days.  This value is subject to normalization in `STAGE` and `PROD` server environments.  This is required.  

- **TRADE_DAY_CLOSING_MINUTE**  

The `TRADE_DAY_CLOSING_MINUTE` value is an integer value from 0 to 59, representing a minute on a 60-minute scale.  The value specifies which minute the trading DAY will close.  Trade day closing is provided for reporting purposes only as a way of breaking up days.  This value is subject to normalization in `STAGE` and `PROD` server environments.  This is required.  

- **TRADE_TICKER_WAITING_SECONDS**  

The `TRADE_TICKER_WAITING_SECONDS` value is an integer representing how many seconds `Go FX` should wait before logging a waiting message while waiting for the next trading window to open.  For example, a value of `3600` will cause the client to log a waiting message every hour with the duration until trading resumes.  This is required.  

- **TRADE_API_ACCOUNT_ID**  

The `TRADE_API_ACCOUNT_ID` is the account ID assigned to your Oanda account.  This value is used in making API calls to the broker.  You likely have a separate account ID for your practice Oanda account versus your live account.  Make sure this account ID corresponds with the correct API token in the next value.  This is required.  

- **TRADE_API_AUTH_HEADER**  

The `TRADE_API_AUTH_HEADER` is the authorization header required to connect with the Oanda broker.  It should always start with `Bearer ` and be followed by your API token (i.e. `Bearer abc123`).  You will have separate API tokens for separate accounts (i.e. live vs. practice).  The API token in this header value must correspond with the account ID in the previous value.  This is required.  

- **TRADE_API_ENDPOINT_GET_SIGNALS**  

The `TRADE_API_ENDPOINT_GET_SIGNALS` value is an Oanda [v20 API endpoint](https://developer.oanda.com/rest-live/forex-labs/#autochartist) in template format.  This endpoint returns Autochartists signals.  Since the default config contains all used endpoints, this value is optional.  

- **TRADE_API_ENDPOINT_GET_ACCOUNT**  

The `TRADE_API_ENDPOINT_GET_ACCOUNT` value is an Oanda [v20 API endpoint](https://developer.oanda.com/rest-live-v20/account-ep/) in template format.  This endpoint returns account information.  Since the default config contains all used endpoints, this value is optional.  

- **TRADE_API_ENDPOINT_CREATE_ORDER**  

The `TRADE_API_ENDPOINT_CREATE_ORDER` value is an Oanda [v20 API endpoint](https://developer.oanda.com/rest-live-v20/order-ep/) in template format.  This endpoint creates orders.  Since the default config contains all used endpoints, this value is optional.  

- **TRADE_API_ENDPOINT_GET_ORDERS**  

The `TRADE_API_ENDPOINT_GET_ORDERS` value is an Oanda [v20 API endpoint](https://developer.oanda.com/rest-live-v20/order-ep/) in template format.  This endpoint returns orders.  Since the default config contains all used endpoints, this value is optional.  

- **TRADE_API_ENDPOINT_UPDATE_ORDER**  

The `TRADE_API_ENDPOINT_UPDATE_ORDER` value is an Oanda [v20 API endpoint](https://developer.oanda.com/rest-live-v20/order-ep/) in template format.  This endpoint updates orders.  Since the default config contains all used endpoints, this value is optional.  

- **TRADE_API_ENDPOINT_CANCEL_ORDER**  

The `TRADE_API_ENDPOINT_CANCEL_ORDER` value is an Oanda [v20 API endpoint](https://developer.oanda.com/rest-live-v20/order-ep/) in template format.  This endpoint cancels an order.  Since the default config contains all used endpoints, this value is optional.  

- **TRADE_API_ENDPOINT_GET_OPEN_TRADES**  

The `TRADE_API_ENDPOINT_GET_OPEN_TRADES` value is an Oanda [v20 API endpoint](https://developer.oanda.com/rest-live-v20/trade-ep/) in template format.  This endpoint returns open trades.  Since the default config contains all used endpoints, this value is optional.  

- **TRADE_API_ENDPOINT_GET_TRADE_BY_ID**  

The `TRADE_API_ENDPOINT_GET_TRADE_BY_ID` value is an Oanda [v20 API endpoint](https://developer.oanda.com/rest-live-v20/trade-ep/) in template format.  This endpoint returns a trade by its ID/identifier.  Since the default config contains all used endpoints, this value is optional.  

- **TRADE_API_ENDPOINT_UPDATE_TRADE_ORDERS**  

The `TRADE_API_ENDPOINT_UPDATE_TRADE_ORDERS` value is an Oanda [v20 API endpoint](https://developer.oanda.com/rest-live-v20/trade-ep/) in template format.  This endpoint updates trade orders (i.e. take profit, stop loss, trailing stop).  Since the default config contains all used endpoints, this value is optional.  

- **TRADE_API_ENDPOINT_CLOSE_TRADE**  

The `TRADE_API_ENDPOINT_CLOSE_TRADE` value is an Oanda [v20 API endpoint](https://developer.oanda.com/rest-live-v20/trade-ep/) in template format.  This endpoint closes 1 to ALL units for a trade.  Since the default config contains all used endpoints, this value is optional.  

- **TRADE_API_ENDPOINT_GET_OPEN_POSITIONS**  

The `TRADE_API_ENDPOINT_GET_OPEN_POSITIONS` value is an Oanda [v20 API endpoint](https://developer.oanda.com/rest-live-v20/position-ep/) in template format.  This endpoint returns open positions.  Since the default config contains all used endpoints, this value is optional.  

- **TRADE_API_ENDPOINT_CLOSE_POSITION**  

The `TRADE_API_ENDPOINT_CLOSE_POSITION` value is an Oanda [v20 API endpoint](https://developer.oanda.com/rest-live-v20/position-ep/) in template format.  This endpoint closes a position.  Since the default config contains all used endpoints, this value is optional.  

- **TRADE_API_ENDPOINT_GET_PRICING**  

The `TRADE_API_ENDPOINT_GET_PRICING` value is an Oanda [v20 API endpoint](https://developer.oanda.com/rest-live-v20/pricing-ep/) in template format.  This endpoint returns real-time pricing information for a given instrument (currency pair).  Since the default config contains all used endpoints, this value is optional.  

- **TRADE_API_ENDPOINT_GET_INSTRUMENTS**  

The `TRADE_API_ENDPOINT_GET_INSTRUMENTS` value is an Oanda [v20 API endpoint](https://developer.oanda.com/rest-live-v20/account-ep/) in template format.  This endpoint returns information about an instrument for the given account.  Since the default config contains all used endpoints, this value is optional.  

<br/>

### **Alerts**
- **ALERT_OPEN_WINDOW_IN_MINUTES**  

The `ALERT_OPEN_WINDOW_IN_MINUTES` value is the integer number of minutes prior to the trading window opening that `Go FX` should send a reminder/alert email.  For example, if the value 30 is set, then 30 minutes prior to the trading window opening, an alert email will be sent as a reminder with the duration to go before trading commences.  This is optional.  
  - Note: if the client is started too late to send the alert (i.e. less than alert minutes before window open) the alert will not be sent.  

- **ALERT_CLOSE_WINDOW_IN_MINUTES**  

The `ALERT_CLOSE_WINDOW_IN_MINUTES` value is the integer number of minutes prior to the trading window closing that `Go FX` should send a reminder/alert email.  For example, if the value 30 is set, then 30 minutes prior to the trading window closing, an alert email will be sent as a reminder with the duration to go before trading stops.  This is optional.  
  - Note: if the client is started too late to send the alert (i.e. less than alert minutes before window close) the alert will not be sent.  

<br/>

### **Provider**
- **PROVIDER_EMAIL_NAME**  

The `PROVIDER_EMAIL_NAME` value is the name of the email provider specified within the [email](./email/email.go) package.  This is the email provider which sends all transactional emails/alerts/reports.  Currently, only `sendgrid` is provided.  This is required.  

- **PROVIDER_EMAIL_AUTH_HEADER**  

The `PROVIDER_EMAIL_AUTH_HEADER` is the authorization header value, which contains the SendGrid API key in the format `Bearer abc123`.  Just like the Oanda auth header value, the SendGrid header is Bearer Auth style with the SendGrid API key after the word `Bearer`.

- **PROVIDER_EMAIL_FROM_ADDRESS**  

The `PROVIDER_EMAIL_FROM_ADDRESS` value is the email address *from* which all app emails will be sent.  

- **PROVIDER_EMAIL_TO_ADDRESS**  

The `PROVIDER_EMAIL_TO_ADDRESS` value is the email address *to* which all app emails will be sent.  

- **PROVIDER_EMAIL_ENDPOINT_SEND_TX**  

The `PROVIDER_EMAIL_ENDPOINT_SEND_TX` value is endpoint for sending transactional emails, not including any request query or body.  

<br/>

# Running Latest Release
It is recommended to run `Go FX` via [Docker](https://docs.docker.com/get-docker/).  

With Docker installed, you can run this application without Git or source code by taking the following steps:
1. Open a terminal or command prompt window and ensure Docker is installed and running:
```sh
$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```
If instead of the above output you see something like `"Is the docker daemon running?"`, then the Docker service isn't running and still needs to be started.

2. Create or choose a local folder/directory to work in.  Create a text file called `dev.env` in that local directory and copy the contents from [this sample env file](./env/.env) into `dev.env`.
3. Fill out all the values for this file according to the [Environment File Setup](#environment-file-setup) section
4. Run the following Docker command to pull down the latest release image to your computer:
```sh
$ docker pull registry.gitlab.com/go-fx/go-fx:latest
```
5. Run the following Docker command to run the image you just downloaded with the environment you configured in your `dev.env` text file:
```sh
$ docker run -d --env-file dev.env --name go-fx registry.gitlab.com/go-fx/go-fx:latest start
```
This command runs a Docker container made from the downloaded image in detached mode (in the background), with auto-removal when the container is stopped, and with the name `go-fx`.  

6. You should see the container running with following command: 
```sh
$ docker ps
CONTAINER ID        IMAGE                      COMMAND             CREATED             STATUS              PORTS               NAMES
9d2c49b2880e        go-fx/go-fx:latest   "./gofx start"      14 seconds ago      Up 13 seconds                           go-fx
```
The name of the container is the last column on the right.

7. To view the logs as the container is running, use the following command:  
```sh
$ docker logs -f go-fx
```
Now you can watch the application in action or exit by hitting `Ctrl + C` and the container will continue to run.

In addition to following the container logs, you will receive email notifications as the application progresses.  You can also monitor any active trades on the official Oanda [desktop](https://www.oanda.com/us-en/trading/platforms/oanda-trade-desktop/), [web](https://trade.oanda.com) or [mobile apps](https://www.oanda.com/us-en/trading/platforms/oanda-trade-mobile/).  

1. To change the environment (i.e. the strategy values), simply save the new changes to `dev.env` and restart the container:
```sh
$ docker stop go-fx && docker rm $_
$ docker run -d --env-file dev.env --name go-fx registry.gitlab.com/go-fx/go-fx:latest start
```

<br/>

# Local Development
Start by forking the [main repo](https://gitlab.com/go-fx/go-fx) and then cloning your fork locally.  Create a copy of the included [.env](env/.env) file within the same `env/` dirctory.  Fill out the environment according to the [Environment File Setup](#environment-file-setup) section above.  

<br/>

## Run Local Build
Using Docker and the included shell script:
```sh
$ sh tools/docker/build_run_local.sh
```
This script takes 2 optional arguments:
  1. If your environment file is named something other than `dev.env`, pass this file name as the first argument
  2. If your release dockerfile is named something other than `release.dockerfile`, pass this as the second argument (requires 1st arg)

This script will build the release docker image and run a container called `go-fx`, following the logs of the container.  You may exit following the logs using `Ctrl + C` but this will not stop the container.  You have to explicitly stop the container and once you do, the container will be auto-removed via the `--rm` flag used in the `docker run` command in the `build_run_local.sh` script

<br/>

## Custom Releases
With the included [GitLab CI configuration file](.gitlab-ci.yml), all you have to do is merge new changes into the `master` branch and the pipeline will publish your own custom Docker release image to your repo's registry.  You can easily modify the registry paths of the [run_release.sh](tools/docker/run_release.sh) script so that you can use this script to run your release either on a machine in the cloud (recommended) or locally for testing purposes.  

The pipeline also publishes a test environment image called `testenv` that is used as the envioronment in which to run the test stages of the pipeline as well.  

## Testing
### Go Test
You may run tests on this project using the standard Go method
```sh
$ go test -v ./...
```

<br/>

### Custom Testing Tool
The `Go-FX` CI uses a custom testing tool, included as a standalone module in this project.  You can find the source for this tool [here](tools/test/testrunner.go).  This tool allows additional options when testing and uses the `go test` command under the hood.  

The main advantage of this tool is the ability to easily ignore some packages via the [testpkg.grepignore](tools/test/testpkg.grepignore) file.  By default the `infra` and `tools` packages are included in this ignore file so that when you run the custom test tool, the `infra` and `tools` packages are ignored but all other packages are tested.  This is useful for customizing code coverage reports:
```sh
$ go run tools/test/testrunner.go -cov
```
Using this tool, packages without tests or ignored packages are excluded from the coverage report and the `total average coverage` produced by this tool reflects that.

<br/>

# Contributing
PR's are welcome!

To contribute to `Go FX`, start by forking the [main repo](https://gitlab.com/go-fx/go-fx) and then cloning your fork locally.  When your changes are pushed to GitLab, open a PR to the `master` branch of the [main repo](https://gitlab.com/go-fx/go-fx).

It is preferred that you name your branch aptly rather than opening a PR from your fork's `master` branch to the main project's `master` branch.  Please use these guidelines when naming your branch:
1. If your PR is adding new functionality, please start the branch name with `feature/...`
2. If your PR is fixing existing functionality, please start the branch name with `hotfix/...`
3. If your PR is adding to the GitLab CI pipeline, please start the branch name with `qa/ci-...`
4. If your PR is adding unit testing to existing functionality, please start the branch name with `qa/test-...`
5. If your PR is editing the README/docs, please start the branch name with `readme/...`

Please make sure your PR only addresses one of the above.  If your PR address more than one, please make separate PR's.  

Unit tests are expected with all fixes and new functionality.  Thanks!  

<br/>

# Authors
[**Go FX**](https://gitlab.com/go-fx/go-fx) was created by @metalgopher

<br/>

# License
[**Go FX**](https://gitlab.com/go-fx/go-fx) is licensed under the [Apache 2.0 license](LICENSE).  

Feel free to use, modify, and distribute!  

<br/>

# Useful Links

## Oanda v20 REST API Reference
- https://developer.oanda.com/rest-live-v20/development-guide/

## Oanda Forex Labs
- https://www1.oanda.com/forex-trading/analysis/labs/

## Buyable Units Calculator
- https://www1.oanda.com/forex-trading/analysis/currency-units-calculator

## Oanda Hours of Operation
- https://www1.oanda.com/resources/legal/united-states/legal/weekend-exposure-limits