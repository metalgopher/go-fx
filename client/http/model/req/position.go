package req

//PutClosePosition is the request model for put request to close position
type PutClosePosition struct {
	LongUnits             string            `json:"longUnits,omitempty"`
	LongClientExtensions  map[string]string `json:"longClientExtensions,omitempty"`
	ShortUnits            string            `json:"shortUnits,omitempty"`
	ShortClientExtensions map[string]string `json:"shortClientExtensions,omitempty"`
}
