package res

import (
	"gofx/model/oanda"
)

//GetAccount is the response model for get account details
type GetAccount struct {
	Account           *oanda.Account `json:"account"`
	LastTransactionID string         `json:"lastTransactionID"`
}

//GetAccountInstruments is the response model for get account instruments
type GetAccountInstruments struct {
	Instruments       []string `json:"instruments"`
	LastTransactionID string   `json:"lastTransactionID"`
}
