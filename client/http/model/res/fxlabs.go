package res

import (
	"gofx/model/oanda"
)

//GetAutoChartistSignals is the response structure for req to get signals
type GetAutoChartistSignals struct {
	Provider string                      `json:"provider"`
	Signals  []*oanda.AutochartistSignal `json:"signals"`
}
