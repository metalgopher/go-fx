package res

import (
	"gofx/model/oanda"
)

//GetPricing is the response model for the get pricing request
type GetPricing struct {
	Prices          []*oanda.ClientPrice     `json:"prices"`
	HomeConversions []*oanda.HomeConversions `json:"homeConversions"`
	TimeRFC3339Nano string                   `json:"time"`
}
