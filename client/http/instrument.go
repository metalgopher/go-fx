package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"gofx/client/http/model/res"
	"gofx/env"
	"gofx/interfaces"
	"gofx/loggers"
	"gofx/model/instruments"
)

//GetInstruments retrieves instrument info for a list of instruments
func GetInstruments(instruments []instruments.Instrument, client interfaces.HTTPClient) (*res.GetInstruments, error) {
	if instruments == nil || len(instruments) < 1 {
		return nil, fmt.Errorf("missing instruments")
	}

	urlParams := url.Values{}
	instrumentNames := make([]string, 0)
	for _, inst := range instruments {
		instrumentNames = append(instrumentNames, inst.Name())
	}
	urlParams.Set("instruments", strings.Join(instrumentNames, ","))

	reqURL := fmt.Sprintf("%s?%s", strings.Replace(env.GetConfig().TradeAPIEndpointGetInstruments, "{accountID}", env.GetConfig().TradeAPIAccountID, 1), urlParams.Encode())
	// ts := loggers.GetTimestamp()
	// loggers.GetJSONLogger().WithField("ts", ts).WithField("reqURL", reqURL).Trace("GetInstruments reqURL")

	instReq, err := http.NewRequest("GET", reqURL, nil)
	instReq.Header.Add("Authorization", env.GetConfig().TradeAPIAuthHeader)
	instReq.Header.Add("Content-Type", "application/json")

	//send request
	response, err := client.Do(instReq)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not retrieve instruments")
		return nil, err
	}

	//parse response body
	var resInst res.GetInstruments
	if resJSON, err := ioutil.ReadAll(response.Body); err == nil {
		if err := json.Unmarshal(resJSON, &resInst); err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not parse get instruments response")
			return nil, err
		}

		if resInst.Instruments == nil {
			err := fmt.Errorf("instruments not received")
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).WithField("instruments", instrumentNames).WithField("res", string(resJSON)).Error(err.Error())
			return nil, err
		}
	} else {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not retrieve instruments")
		return nil, err
	}

	return &resInst, nil
}
