package client

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"

	gofxhttp "gofx/client/http"
	"gofx/client/http/model/res"
	"gofx/client/util"
	"gofx/db"
	"gofx/email"
	"gofx/env"
	"gofx/loggers"
	"gofx/model"
	"gofx/model/oanda"
	"gofx/reporting"
	"gofx/strategy"
)

func runTradingFlow(strat strategy.Strategy, wg *sync.WaitGroup) {
	//log strategy name
	logger.Printf("Running strategy '%s'...\n\n", ActiveStrategy.Name())

	//find next rounded tick time
	var nextTickTime time.Time
	if time.Now().Round(strat.Interval()).Before(time.Now()) {
		nextTickTime = time.Now().Add(strat.Interval()).Round(strat.Interval())
	} else {
		nextTickTime = time.Now().Round(strat.Interval())
	}
	//wait for next rounded tick time for alignment
	<-time.After(time.Until(nextTickTime) + time.Millisecond*time.Duration(100))

	//start aligned ticker for strategy
	ticker := time.NewTicker(strat.Interval())

	//run initial interval
	endExecution := runFlowInterval(strat)

	//log (with delay to ensure it prints at end of interval)
	time.Sleep(time.Second)
	logger.Printf("End interval for strategy '%s' ...\n\n", ActiveStrategy.Name())

	//handle ending execution
	if endExecution {
		//stop ticker
		ticker.Stop()

		//start trading client again
		wg.Add(1)
		go Run()

		//exit this trading flow
		wg.Done()
		return
	}

	//run intervals until trading window close or exit
	for {
		select {
		case <-ticker.C:
			endExecution := runFlowInterval(strat)

			//log (with delay to ensure it prints at end of interval)
			time.Sleep(time.Second)
			logger.Printf("End interval for strategy '%s' ...\n\n", ActiveStrategy.Name())

			//handle ending execution
			if endExecution {
				//stop ticker
				ticker.Stop()

				//start trading client again
				wg.Add(1)
				go Run()

				//exit this trading flow
				wg.Done()
				return
			}
		}
	}
}

func runFlowInterval(strat strategy.Strategy) bool {
	//take current time
	current := time.Now()

	//log
	logger.Printf("Begin interval for strategy '%s' ...", ActiveStrategy.Name())

	//1. create signal trade(s) if any orders were filled on broker that resulted in trades
	err := convertFilledOrdersToSignalTrades()
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not convert filled orders to signal trades")
	}

	//2. close signal trade for broker if past deadline in DB
	err = closeBrokerPastDeadlineTrades()
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not close past deadline signal trades")
	}

	//3. close signal trade for DB if closed on broker
	err = closeDBCompletedSignalTrades()
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not close complete signal trades")
	}

	//4. check for daily/weekly closing...

	// -- daily --
	closeToday := time.Date(current.Year(), current.Month(), current.Day(), env.GetConfig().TradeDayClosingHour, env.GetConfig().TradeDayClosingMinute, 0, 0, time.Local).Add(-time.Second)
	if closeToday.Before(current.Round(strat.Interval())) && current.Round(strat.Interval()).Add(-strat.Interval()).Before(closeToday) {
		//send daily report
		baseData := email.BaseTemplateData{
			AppName:       env.GetConfig().AppName,
			CompanyName:   env.GetConfig().AppCompany,
			CompanyLink:   env.GetConfig().AppCompanyLink,
			CopyrightYear: current.Year(),
			StrategyName:  strat.Name(),
		}
		go reporting.SendDailySummaryReportEmail(current.Round(strat.Interval()),
			env.GetConfig().ProviderEmailToAddress, env.GetConfig().ProviderEmailFromAddress, baseData, &http.Client{}, nil)
	}

	// -- weekly --
	_, windowClose, currentWithin := util.GetCurrentTradingWindow(current.Round(strat.Interval()))
	_, _, prevWithin := util.GetCurrentTradingWindow(current.Round(strat.Interval()).Add(-strat.Interval()))

	if !currentWithin && prevWithin {
		//send week ending report(s)
		baseData := email.BaseTemplateData{
			AppName:       env.GetConfig().AppName,
			CompanyName:   env.GetConfig().AppCompany,
			CompanyLink:   env.GetConfig().AppCompanyLink,
			CopyrightYear: current.Round(strat.Interval()).Year(),
			StrategyName:  strat.Name(),
		}
		//send week-ending daily report if necessary
		if env.GetConfig().TradeDayClosingHour != env.GetConfig().TradeWindowCloseHour || env.GetConfig().TradeDayClosingMinute != env.GetConfig().TradeWindowCloseMinute {
			reporting.SendWeekEndingDailySummaryReportEmail(current.Round(strat.Interval()),
				env.GetConfig().ProviderEmailToAddress, env.GetConfig().ProviderEmailFromAddress, baseData, &http.Client{}, nil)
		}
		//send weekly report
		reporting.SendWeeklySummaryReportEmail(current.Round(strat.Interval()),
			env.GetConfig().ProviderEmailToAddress, env.GetConfig().ProviderEmailFromAddress, baseData, &http.Client{}, nil)

		//week over
		logger.Printf("Current Week Has Ended!\n\n")
		logger.Printf("Now restarting trading flow...\n\n")

		return true
	}

	//5. check if window closing alert should be sent
	if alertTime := windowClose.Add(-time.Minute * time.Duration(env.GetConfig().AlertCloseWindowInMinutes)); env.GetConfig().AlertOpenWindowInMinutes > 0 &&
		alertTime.Before(current.Round(strat.Interval())) && current.Round(strat.Interval()).Add(-strat.Interval()).Before(alertTime) {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).Infof("sending alert that trading window will close in %v", time.Until(windowClose).Round(time.Minute))

		//get account data
		if acct, err := gofxhttp.GetAccount(&http.Client{}); err == nil {
			//alert data
			alertData := email.WindowAlertTemplateData{}
			alertData.AccountTemplateData = *email.NewAccountTemplateData(acct)
			alertData.AppName = env.GetConfig().AppName
			alertData.CompanyLink = env.GetConfig().AppCompanyLink
			alertData.CompanyName = env.GetConfig().AppCompany
			alertData.CopyrightYear = current.Round(strat.Interval()).Year()
			alertData.StrategyName = strat.Name()
			alertData.TypeTitle = "Closing"
			alertData.TypeLower = "closing"
			alertData.WindowTimeRFC1123 = windowClose.Format(time.RFC1123)
			alertData.DurationUntilWindowTime = fmt.Sprintf("%dm", env.GetConfig().AlertCloseWindowInMinutes)

			email.ActiveProvider.SendTXWindowAlert(env.GetConfig().ProviderEmailToAddress, env.GetConfig().ProviderEmailFromAddress, fmt.Sprintf("Trading Window %s Soon", alertData.TypeTitle), alertData, &http.Client{}, nil)
		} else {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("error sending alert that trading window will close")
		}
	}

	//6. gather signal data
	params := strat.SignalReqParams()
	signals := gofxhttp.GetAutochartistSignals(params, &http.Client{})

	//log
	signalJSON, _ := json.Marshal(signals)
	ts := loggers.GetTimestamp()
	loggers.GetJSONLogger().WithField("ts", ts).Infof("%d signals retrieved", len(signals))
	loggers.GetJSONLogger().WithField("ts", ts).WithField("signals", string(signalJSON)).Trace("retrieved")

	//7. Run strategy and process resulting actions
	actions := strat.Run(signals)
	if actions == nil || len(actions) == 0 {
		return false
	}

	//8. Execute actions
	for _, action := range actions {
		switch action.(type) {
		case *strategy.CreateMarketTrade:
			action := action.(*strategy.CreateMarketTrade)
			if _, err := createSignalTradeFromMarketOrder(action.Trade, &http.Client{}); err != nil {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not complete action ... %s", action.Description())
				break
			}
		case *strategy.CreateUpdateDeleteTradeOrders:
			action := action.(*strategy.CreateUpdateDeleteTradeOrders)
			if _, err := createUpdateDeleteTradeOrders(action.Trade, &http.Client{}); err != nil {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not complete action ... %s", action.Description())
			}
		case *strategy.CloseTradeUnits:
			action := action.(*strategy.CloseTradeUnits)
			if _, err := closeSignalTradeUnits(action.TradeSpecifier, action.Units, &http.Client{}); err != nil {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not complete action ... %s", action.Description())
			}
		case *strategy.CreateLimitOrder:
			action := action.(*strategy.CreateLimitOrder)
			if _, err := createLimitSignalOrder(action.Order, &http.Client{}); err != nil {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not complete action ... %s", action.Description())
			}
		case *strategy.CreateStopOrder:
			action := action.(*strategy.CreateStopOrder)
			if _, err := createStopSignalOrder(action.Order, &http.Client{}); err != nil {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not complete action ... %s", action.Description())
			}
		}
	}

	//continue flow, no restart
	return false
}

func convertFilledOrdersToSignalTrades() error {
	// -- get unfilled strategy orders --
	orders, err := db.GetUnfilledStrategySignalOrders(ActiveStrategy.Name(), nil)
	if err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not get signal orders from db")
		return err
	}

	if len(orders) < 1 {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).Info("no unfilled orders found in db")
		return nil
	}

	orderCount := len(orders)
	if orderCount > 500 {
		orderCount = 500
	}

	//get details from Oanda on orders
	orderIDs := make([]string, 0)
	for _, order := range orders {
		orderIDs = append(orderIDs, order.OrderID)
	}
	ordersParams := url.Values{}
	ordersParams.Set("ids", strings.Join(orderIDs, ","))
	ordersParams.Set("count", strconv.Itoa(orderCount)) //max 500

	//broker API call
	var resOrders []*oanda.AccountOrder
	var resErr error
	for i := 0; i < 3; i++ {
		resOrders, resErr = gofxhttp.GetOrders(ordersParams, &http.Client{})
		if resErr != nil {
			time.Sleep(time.Second * 5)
			continue
		}
		break
	}
	if resErr != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not get orders info from broker for db order-to-trade conversion")
		return resErr
	}

	//update order(s) if necessary
	for _, acctOrder := range resOrders {
		if acctOrder.FilledTime != nil {
			orders[acctOrder.ID].FilledAt = acctOrder.FilledTime
		}
		if acctOrder.TradeOpenedID != nil {
			orders[acctOrder.ID].TradeID = acctOrder.TradeOpenedID

			//create trade
			trade := &model.SignalTrade{}
			trade.TradeID = *orders[acctOrder.ID].TradeID
			trade.Signal = &model.AutochartistSignal{}
			trade.Signal.ID = orders[acctOrder.ID].Signal.ID
			trade.StrategyName = orders[acctOrder.ID].StrategyName
			trade.Instrument = orders[acctOrder.ID].Instrument
			trade.Units = orders[acctOrder.ID].Units
			price, _ := strconv.ParseFloat(acctOrder.Price, 64)
			trade.EntryPrice = price
			trade.EntryTime = *acctOrder.FilledTime
			trade.Deadline = orders[acctOrder.ID].Deadline
			trade.TakePrice = orders[acctOrder.ID].TakePrice
			trade.StopPrice = orders[acctOrder.ID].StopPrice
			trade.TSDistance = orders[acctOrder.ID].TSDistance

			//add trade to db
			if err := db.CreateSignalTrade(trade, nil); err != nil {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not add trade to db")
				continue
			}

			//log
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithField("openedTrade", trade).Tracef("Opened trade ID '%s' details", trade.TradeID)
			loggers.GetJSONLogger().WithField("ts", ts).Infof("BROKER UPDATE! ... Opened trade ID '%s' in db", trade.TradeID)

			//send email alert
			emailData := email.OpenTradeTemplateData{}
			emailData.AppName = env.GetConfig().AppName
			emailData.CompanyLink = env.GetConfig().AppCompanyLink
			emailData.CompanyName = env.GetConfig().AppCompany
			emailData.CopyrightYear = time.Now().Year()
			emailData.StrategyName = trade.StrategyName
			emailData.Trade = trade
			if emailData.Trade.Deadline != nil {
				emailData.Trade.DeadlineRFC1123 = trade.Deadline.Format(time.RFC1123)
			}

			//get account email data
			resAcct, err := gofxhttp.GetAccount(&http.Client{})
			if err != nil {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not send open trade alert email")
				continue
			}
			emailData.AccountTemplateData = *email.NewAccountTemplateData(resAcct)

			_, err = email.ActiveProvider.SendTXOpenTrade(env.GetConfig().ProviderEmailToAddress, env.GetConfig().ProviderEmailFromAddress, emailData, &http.Client{}, nil)
			if err != nil {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not send open trade alert email")
			}
			ts = loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).Infof("successfully sent open trade alert email for trade ID '%s", trade.TradeID)
		}

		//update order in db
		if acctOrder.FilledTime != nil || acctOrder.TradeOpenedID != nil {
			if err := db.UpdateSignalOrder(orders[acctOrder.ID], nil); err != nil {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Error("could not udpate db order")
				continue
			}

			//log
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).Infof("BROKER UPDATE! ... Updated order ID '%s' in db", acctOrder.ID)
		}
	}

	return nil
}

func closeBrokerPastDeadlineTrades() error {
	var firstErr error

	openTrades, err := db.GetOpenStrategySignalTrades(ActiveStrategy.Name(), nil)
	if err != nil {
		return err
	}

	for _, signalTrade := range openTrades {
		//check deadline
		if time.Now().Before(*signalTrade.Deadline) {
			//not past deadline
			continue
		}

		//close past due trade
		_, err := closeSignalTradeUnits(signalTrade.TradeID, "ALL", &http.Client{})
		if err != nil {
			firstErr = err
			continue
		}

		//log
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).Infof("BROKER UPDATE! ... Closed trade ID '%s' with broker, past deadline @ %v",
			signalTrade.TradeID, signalTrade.Deadline.Format(time.RFC1123))
	}

	return firstErr
}

func closeDBCompletedSignalTrades() error {
	openTrades, err := db.GetOpenStrategySignalTrades(ActiveStrategy.Name(), nil)
	if err != nil {
		return err
	}

	for _, signalTrade := range openTrades {
		var resTrade *res.GetTrade
		var resTradeErr error
		for i := 0; i < 3; i++ {
			resTrade, resTradeErr = gofxhttp.GetTradeByID(signalTrade.TradeID, &http.Client{})
			if resTradeErr != nil {
				time.Sleep(time.Second * 5)
				continue
			}
			break
		}
		if resTradeErr != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(resTradeErr).Errorf("could not retrieve trade for db closing, ID: %s", signalTrade.TradeID)
			continue
		}

		if resTrade.Trade.StateName == "CLOSED" {
			//update trade for closing
			signalTrade.ExitTime = resTrade.Trade.CloseTime
			signalTrade.ExitTimeRFC1123 = signalTrade.ExitTime.Format(time.RFC1123)
			avgClosePrice, _ := strconv.ParseFloat(resTrade.Trade.AverageClosePrice, 64)
			signalTrade.ExitPrice = &avgClosePrice
			profit, _ := strconv.ParseFloat(resTrade.Trade.RealizedPL, 64)
			signalTrade.ProfitLoss = &profit

			//update signal trade in db
			if err := db.UpdateSignalTrade(signalTrade, nil); err != nil {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not close completed db tradeID '%s'", signalTrade.TradeID)
				continue
			}

			//log
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).Infof("BROKER UPDATE! ... Closed completed trade ID '%s' in db", signalTrade.TradeID)

			//send email alert
			emailData := email.CloseTradeTemplateData{}
			emailData.AppName = env.GetConfig().AppName
			emailData.CompanyLink = env.GetConfig().AppCompanyLink
			emailData.CompanyName = env.GetConfig().AppCompany
			emailData.CopyrightYear = time.Now().Year()
			emailData.StrategyName = signalTrade.StrategyName
			emailData.Trade = signalTrade
			if *signalTrade.ProfitLoss > 0 {
				emailData.Result = "PROFIT"
			} else if *signalTrade.ProfitLoss < 0 {
				emailData.Result = "LOSS"
			} else {
				emailData.Result = "NO GAIN"
			}

			//get account email data
			resAcct, err := gofxhttp.GetAccount(&http.Client{})
			if err != nil {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not send close trade alert email for tradeID '%s'", signalTrade.TradeID)
				continue
			}
			emailData.AccountTemplateData = *email.NewAccountTemplateData(resAcct)

			_, err = email.ActiveProvider.SendTXCloseTrade(env.GetConfig().ProviderEmailToAddress, env.GetConfig().ProviderEmailFromAddress, emailData, &http.Client{}, nil)
			if err != nil {
				ts := loggers.GetTimestamp()
				loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not send close trade alert email for tradeID '%s'", signalTrade.TradeID)
				continue
			}
			ts = loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).Infof("successfully sent close trade alert email for trade ID '%s", signalTrade.TradeID)
		}
	}

	return nil
}
