# this file sets up test environment image

# caches go mod deps
FROM golang:1.14-alpine AS mod

WORKDIR /app

COPY . .

RUN go mod download

# final test image
FROM golang:1.14

# add apt key for LLVM repository
RUN wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -

# add LLVM apt repository
RUN echo "deb http://apt.llvm.org/stretch/ llvm-toolchain-stretch-5.0 main" | tee -a /etc/apt/sources.list

# install clang from LLVM repository
RUN apt-get update && apt-get install -y --no-install-recommends \
    clang-6.0 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# set Clang as default CC
ENV set_clang /etc/profile.d/set-clang-cc.sh
ENV CC clang-6.0
RUN echo "export CC=clang-6.0" | tee -a ${set_clang} && chmod a+x ${set_clang}

# install golint
RUN go get -u golang.org/x/lint/golint
RUN chmod +x $GOPATH/bin/golint

# args
ARG CI_PROJECT_PATH

# env
ENV GOOS linux
ENV GOARCH amd64
ENV CGO_ENABLED 1
ENV CI_PROJECT_PATH $CI_PROJECT_PATH

# copy mod dep cache from mod build stage
COPY --from=mod $GOCACHE $GOCACHE
COPY --from=mod $GOPATH/pkg/mod $GOPATH/pkg/mod

ENV APP_PATH $GOPATH/src/$CI_PROJECT_PATH
WORKDIR $APP_PATH

# copy working dir into image
COPY . .

# build to ensure health
RUN cd tools/test && CGO_ENABLED=0 GOOS=$GOOS GOARCH=amd64 go build -a -installsuffix cgo -ldflags="-w -s" .

# build testrunner tool for running test commands in this image
RUN chmod +x ./tools/test/testrunner

CMD go test -v ./...
