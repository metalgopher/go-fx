# Optional first argument for environment file name within env directory, else defaults to dev.env
ENV_FILE=$1
if [ -z "$1" ]
then
    ENV_FILE="dev.env"
fi

docker pull registry.gitlab.com/go-fx/go-fx:latest

docker run --rm -d --env-file env/$ENV_FILE --name go-fx registry.gitlab.com/go-fx/go-fx:latest start

docker logs -f go-fx
