# Optional first argument for environment file name within env directory, else defaults to dev.env
ENV_FILE=$1
if [ -z "$1" ]
then
    ENV_FILE="dev.env"
fi

# Optional second argument for dockerfile name within tools/docker directory, else defaults to release.dockerfile
DOCKER_FILE=$2
if [ -z "$2" ]
then
    DOCKER_FILE="testenv.dockerfile"
fi

docker build -t go-fx/go-fx:test -f tools/docker/dockerfiles/$DOCKER_FILE . || exit 1

docker run --rm --env-file server/env/$ENV_FILE --name go-fx-test go-fx/go-fx:test
