package db

import (
	"fmt"
)

//ErrUniqueFieldViolation is a db error where an insert was attempted that violates a unique field constraint
var ErrUniqueFieldViolation error

//DuplicateValueError is an error from attempting to add a duplicate value to a unique field
type DuplicateValueError struct {
	FieldName string
}

func (e DuplicateValueError) Error() string {
	return fmt.Sprintf("violation of unique constraint on field '%s'", e.FieldName)
}

//Is supports xerror error equivalence checking
func (e DuplicateValueError) Is(other error) bool {
	_, ok := other.(DuplicateValueError)
	return ok
}
