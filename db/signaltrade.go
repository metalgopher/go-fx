package db

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"gofx/loggers"
	"gofx/model"
	"gofx/model/instruments"
)

//CreateSignalTradeWithSignal creates a new signal trade in the db along with its associated signal
func CreateSignalTradeWithSignal(trade *model.SignalTrade, db *sql.DB) error {
	if db == nil {
		db = conn
	}

	//marshal full signal to JSON
	rawJSON, _ := json.Marshal(trade.Signal)

	//optional prediction values
	var priceLow, priceHigh *float64
	var timeFrom, timeTo *time.Time
	if trade.Signal.Data.Prediction != nil {
		priceLow = trade.Signal.Data.Prediction.PriceLow
		priceHigh = trade.Signal.Data.Prediction.PriceHigh
		timeFrom = trade.Signal.Data.Prediction.TimeFrom
		timeTo = trade.Signal.Data.Prediction.TimeTo
	}

	//insert signal
	_, err := db.Exec(`INSERT INTO autochartist_signals(id, signal_type, instrument, completed, probability, 
		signal_interval, direction, pattern, price_low, price_high, time_from, time_to, raw_data) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
		trade.Signal.ID, trade.Signal.Type, trade.Signal.Instrument.Name(), trade.Signal.Meta.Completed, trade.Signal.Meta.Probability,
		trade.Signal.Meta.Interval, trade.Signal.Meta.Direction, trade.Signal.Meta.Pattern, priceLow, priceHigh, timeFrom, timeTo, string(rawJSON))
	if err != nil {
		return fmt.Errorf("could not create new signal >> %w", err)
	}

	//insert trade
	_, err = db.Exec("INSERT INTO signal_trades(trade_id, signal_id, strategy_name, instrument, units, entry_price, entry_time, deadline, take_price, stop_price, ts_distance) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
		trade.TradeID, trade.Signal.ID, trade.StrategyName, trade.Instrument.Name(), trade.Units, trade.EntryPrice, trade.EntryTime, trade.Deadline, trade.TakePrice, trade.StopPrice, trade.TSDistance)
	if err != nil {
		return fmt.Errorf("could not create new trade >> %w", err)
	}

	return nil
}

//CreateSignalTrade creates a new signal trade in the db
func CreateSignalTrade(trade *model.SignalTrade, db *sql.DB) error {
	if db == nil {
		db = conn
	}

	//insert trade
	_, err := db.Exec("INSERT INTO signal_trades(trade_id, signal_id, strategy_name, instrument, units, entry_price, entry_time, deadline, take_price, stop_price, ts_distance) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
		trade.TradeID, trade.Signal.ID, trade.StrategyName, trade.Instrument.Name(), trade.Units, trade.EntryPrice, trade.EntryTime, trade.Deadline, trade.TakePrice, trade.StopPrice, trade.TSDistance)
	if err != nil {
		return fmt.Errorf("could not create new trade >> %w", err)
	}

	return nil
}

//GetOpenStrategySignalTrades retrieves all signal trades for a given strategy
func GetOpenStrategySignalTrades(strategyName string, db *sql.DB) (map[string]*model.SignalTrade, error) {
	return getSignalTrades(true, &strategyName, db)
}

//GetOpenSignalTrades retrieves all signal trades
func GetOpenSignalTrades(db *sql.DB) (map[string]*model.SignalTrade, error) {
	return getSignalTrades(true, nil, db)
}

//GetAllSignalTrades retrieves all signal trades
func GetAllSignalTrades(db *sql.DB) (map[string]*model.SignalTrade, error) {
	return getSignalTrades(false, nil, db)
}

func getSignalTrades(filterOpen bool, strategyName *string, db *sql.DB) (map[string]*model.SignalTrade, error) {
	if db == nil {
		db = conn
	}

	query := strings.Builder{}
	if filterOpen {
		query.WriteString(`
			SELECT trade_id, signal_id, strategy_name, trades.instrument, units, entry_price, entry_time, deadline, take_price,
				stop_price, ts_distance, exit_price, exit_time, profit_loss, trades.created_at, trades.updated_at, raw_data
			FROM signal_trades AS trades
				JOIN autochartist_signals AS signals ON trades.signal_id = signals.id
			WHERE trades.deleted_at IS NULL AND trades.exit_time IS NULL`)
		if strategyName != nil {
			query.WriteString(fmt.Sprintf(" AND strategy_name = '%s'", *strategyName))
		}
	} else {
		query.WriteString(`
			SELECT trade_id, signal_id, strategy_name, trades.instrument, units, entry_price, entry_time, deadline, take_price,
				stop_price, ts_distance, exit_price, exit_time, profit_loss, trades.created_at, trades.updated_at, raw_data
			FROM signal_trades AS trades
				JOIN autochartist_signals AS signals ON trades.signal_id = signals.id
			WHERE trades.deleted_at IS NULL`)
		if strategyName != nil {
			query.WriteString(fmt.Sprintf(" AND strategy_name = '%s'", *strategyName))
		}
	}
	query.WriteString(" ORDER BY trades.trade_id")

	var rows *sql.Rows
	var err error
	rows, err = db.Query(query.String())
	if err != nil {
		return nil, fmt.Errorf("could not get signal trades >> %w", err)
	}

	results := make(map[string]*model.SignalTrade)
	defer rows.Close()

	for rows.Next() {
		if rows.Err() != nil {
			return nil, rows.Err()
		}

		//column vars
		var tradeID string
		var signalID int
		var strategyName string
		var instrument string
		var units int
		var entryPrice float64
		var entryTime time.Time
		var deadline sql.NullTime
		var takePrice sql.NullFloat64
		var stopPrice sql.NullFloat64
		var tsDistance sql.NullFloat64
		var exitPrice sql.NullFloat64
		var exitTime sql.NullTime
		var profitLoss sql.NullFloat64
		var createdAt time.Time
		var updatedAt time.Time
		var rawData string
		if err := rows.Scan(&tradeID, &signalID, &strategyName, &instrument, &units, &entryPrice, &entryTime, &deadline, &takePrice,
			&stopPrice, &tsDistance, &exitPrice, &exitTime, &profitLoss, &createdAt, &updatedAt, &rawData); err != nil {
			return nil, err
		}

		//build trade
		trade := &model.SignalTrade{}
		trade.TradeID = tradeID
		trade.StrategyName = strategyName
		trade.Instrument = instruments.ParseInstrument(instrument)
		trade.Units = units
		trade.EntryPrice = entryPrice
		trade.EntryTime = entryTime
		trade.EntryTimeRFC1123 = trade.EntryTime.Format(time.RFC1123)
		if takePrice.Valid {
			trade.TakePrice = &takePrice.Float64
		}
		if deadline.Valid {
			trade.Deadline = &deadline.Time
			trade.DeadlineRFC1123 = trade.Deadline.Format(time.RFC1123)
		}
		if stopPrice.Valid {
			trade.StopPrice = &stopPrice.Float64
		}
		if tsDistance.Valid {
			trade.TSDistance = &tsDistance.Float64
		}
		if exitPrice.Valid {
			trade.ExitPrice = &exitPrice.Float64
		}
		if exitTime.Valid {
			trade.ExitTime = &exitTime.Time
			trade.ExitTimeRFC1123 = trade.ExitTime.Format(time.RFC1123)
		}
		if profitLoss.Valid {
			trade.ProfitLoss = &profitLoss.Float64
		}
		trade.CreatedAt = &createdAt
		trade.UpdatedAt = &updatedAt
		trade.Signal = &model.AutochartistSignal{}
		if err := json.Unmarshal([]byte(rawData), trade.Signal); err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not unmarshal trade id: %s", tradeID)
		}

		//add to results
		results[trade.TradeID] = trade
	}

	return results, nil
}

//GetSignalTradeByID retrieves a signal trade by its id
func GetSignalTradeByID(tradeID string, db *sql.DB) (*model.SignalTrade, error) {
	if db == nil {
		db = conn
	}

	query := `
		SELECT signal_id, strategy_name, instrument, units, entry_price, entry_time, deadline, take_price, 
			stop_price, ts_distance, exit_price, exit_time, profit_loss, trades.created_at, trades.updated_at, trades.deleted_at, raw_data
		FROM signal_trades AS trades
			JOIN autochartist_signals AS signals ON trades.signal_id = signals.id
		WHERE trades.trade_id = ?`

	result := db.QueryRow(query, tradeID)

	//column vars
	var signalID int
	var strategyName string
	var instrument string
	var units int
	var entryPrice float64
	var entryTime time.Time
	var deadline sql.NullTime
	var takePrice sql.NullFloat64
	var stopPrice sql.NullFloat64
	var tsDistance sql.NullFloat64
	var exitPrice sql.NullFloat64
	var exitTime sql.NullTime
	var profitLoss sql.NullFloat64
	var createdAt time.Time
	var updatedAt time.Time
	var deletedAt sql.NullTime
	var rawData string
	if err := result.Scan(&signalID, &strategyName, &instrument, &units, &entryPrice, &entryTime, &deadline, &takePrice,
		&stopPrice, &tsDistance, &exitPrice, &exitTime, &profitLoss, &createdAt, &updatedAt, &deletedAt, &rawData); err != nil {
		return nil, err
	}

	//build trade result
	trade := &model.SignalTrade{}
	trade.TradeID = tradeID
	trade.StrategyName = strategyName
	trade.Instrument = instruments.ParseInstrument(instrument)
	trade.Units = units
	trade.EntryPrice = entryPrice
	trade.EntryTime = entryTime
	if deadline.Valid {
		trade.Deadline = &deadline.Time
	}
	if takePrice.Valid {
		trade.TakePrice = &takePrice.Float64
	}
	if stopPrice.Valid {
		trade.StopPrice = &stopPrice.Float64
	}
	if tsDistance.Valid {
		trade.TSDistance = &tsDistance.Float64
	}
	if exitPrice.Valid {
		trade.ExitPrice = &exitPrice.Float64
	}
	if exitTime.Valid {
		trade.ExitTime = &exitTime.Time
	}
	if profitLoss.Valid {
		trade.ProfitLoss = &profitLoss.Float64
	}
	trade.CreatedAt = &createdAt
	trade.UpdatedAt = &updatedAt
	if deletedAt.Valid {
		trade.DeletedAt = &deletedAt.Time
	}
	if err := json.Unmarshal([]byte(rawData), trade.Signal); err != nil {
		ts := loggers.GetTimestamp()
		loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not unmarshal signal for trade id: %s", tradeID)
	}

	return trade, nil
}

//GetSignalTradesBySignalID retrieves all signal trades associated with a certain signal
func GetSignalTradesBySignalID(signalID int, db *sql.DB) (map[string]*model.SignalTrade, error) {
	if db == nil {
		db = conn
	}

	query := `
		SELECT trade_id, strategy_name, trades.instrument, units, entry_price, entry_time, deadline, take_price, 
			stop_price, ts_distance, exit_price, exit_time, profit_loss, trades.created_at, trades.updated_at, raw_data
		FROM signal_trades AS trades
			JOIN autochartist_signals AS signals ON trades.signal_id = signals.id
		WHERE trades.deleted_at IS NULL AND trades.signal_id = ?`

	rows, err := db.Query(query, signalID)
	if err != nil {
		return nil, fmt.Errorf("could not get signal trades >> %w", err)
	}

	results := make(map[string]*model.SignalTrade)
	defer rows.Close()

	for rows.Next() {
		if rows.Err() != nil {
			return nil, rows.Err()
		}

		//column vars
		var tradeID string
		var strategyName string
		var instrument string
		var units int
		var entryPrice float64
		var entryTime time.Time
		var deadline sql.NullTime
		var takePrice sql.NullFloat64
		var stopPrice sql.NullFloat64
		var tsDistance sql.NullFloat64
		var exitPrice sql.NullFloat64
		var exitTime sql.NullTime
		var profitLoss sql.NullFloat64
		var createdAt time.Time
		var updatedAt time.Time
		var rawData string
		if err := rows.Scan(&tradeID, &strategyName, &instrument, &units, &entryPrice, &entryTime, &deadline, &takePrice,
			&stopPrice, &tsDistance, &exitPrice, &exitTime, &profitLoss, &createdAt, &updatedAt, &rawData); err != nil {
			return nil, err
		}

		//build trade
		trade := &model.SignalTrade{}
		trade.TradeID = tradeID
		trade.StrategyName = strategyName
		trade.Instrument = instruments.ParseInstrument(instrument)
		trade.Units = units
		trade.EntryPrice = entryPrice
		trade.EntryTime = entryTime
		if takePrice.Valid {
			trade.TakePrice = &takePrice.Float64
		}
		if deadline.Valid {
			trade.Deadline = &deadline.Time
		}
		if stopPrice.Valid {
			trade.StopPrice = &stopPrice.Float64
		}
		if tsDistance.Valid {
			trade.TSDistance = &tsDistance.Float64
		}
		if exitPrice.Valid {
			trade.ExitPrice = &exitPrice.Float64
		}
		if exitTime.Valid {
			trade.ExitTime = &exitTime.Time
		}
		if profitLoss.Valid {
			trade.ProfitLoss = &profitLoss.Float64
		}
		trade.CreatedAt = &createdAt
		trade.UpdatedAt = &updatedAt
		trade.Signal = &model.AutochartistSignal{}
		if err := json.Unmarshal([]byte(rawData), trade.Signal); err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not unmarshal trade id: %s", tradeID)
		}

		//add to results
		results[trade.TradeID] = trade
	}

	return results, nil
}

//GetStrategyTradesByTimeRange retrieves any trades of a given strategy which were "alive" over a given time range
func GetStrategyTradesByTimeRange(strategyName string, fromTime, toTime time.Time, db *sql.DB) (map[string]*model.SignalTrade, error) {
	if db == nil {
		db = conn
	}

	query := `
		SELECT trade_id, signal_id, strategy_name, trades.instrument, units, entry_price, entry_time, deadline, take_price, 
		stop_price, ts_distance, exit_price, exit_time, profit_loss, trades.created_at, trades.updated_at, raw_data
		FROM signal_trades AS trades
			JOIN autochartist_signals AS signals ON trades.signal_id = signals.id
		WHERE trades.strategy_name = ? AND trades.deleted_at IS NULL AND (
			trades.entry_time BETWEEN ? AND ? OR
			trades.exit_time BETWEEN ? AND ? OR
			trades.entry_time <= ? AND trades.exit_time IS NULL OR
			trades.entry_time <= ? AND trades.exit_time >= ?
		)`

	rows, err := db.Query(query, strategyName, fromTime, toTime, fromTime, toTime, toTime, toTime, fromTime)
	if err != nil {
		return nil, fmt.Errorf("could not get signal trades >> %w", err)
	}

	results := make(map[string]*model.SignalTrade)
	defer rows.Close()

	for rows.Next() {
		if rows.Err() != nil {
			return nil, rows.Err()
		}

		//column vars
		var tradeID string
		var signalID int
		var strategyName string
		var instrument string
		var units int
		var entryPrice float64
		var entryTime time.Time
		var deadline sql.NullTime
		var takePrice sql.NullFloat64
		var stopPrice sql.NullFloat64
		var tsDistance sql.NullFloat64
		var exitPrice sql.NullFloat64
		var exitTime sql.NullTime
		var profitLoss sql.NullFloat64
		var createdAt time.Time
		var updatedAt time.Time
		var rawData string
		if err := rows.Scan(&tradeID, &signalID, &strategyName, &instrument, &units, &entryPrice, &entryTime, &deadline, &takePrice,
			&stopPrice, &tsDistance, &exitPrice, &exitTime, &profitLoss, &createdAt, &updatedAt, &rawData); err != nil {
			return nil, err
		}

		//build trade
		trade := &model.SignalTrade{}
		trade.TradeID = tradeID
		trade.StrategyName = strategyName
		trade.Instrument = instruments.ParseInstrument(instrument)
		trade.Units = units
		trade.EntryPrice = entryPrice
		trade.EntryTime = entryTime
		trade.EntryTimeRFC1123 = entryTime.Format(time.RFC1123)
		if deadline.Valid {
			trade.Deadline = &deadline.Time
			trade.DeadlineRFC1123 = trade.Deadline.Format(time.RFC1123)
		}
		if takePrice.Valid {
			trade.TakePrice = &takePrice.Float64
		}
		if stopPrice.Valid {
			trade.StopPrice = &stopPrice.Float64
		}
		if tsDistance.Valid {
			trade.TSDistance = &tsDistance.Float64
		}
		if exitPrice.Valid {
			trade.ExitPrice = &exitPrice.Float64
		}
		if exitTime.Valid {
			trade.ExitTime = &exitTime.Time
			trade.ExitTimeRFC1123 = trade.ExitTime.Format(time.RFC1123)
		}
		if profitLoss.Valid {
			trade.ProfitLoss = &profitLoss.Float64
		}
		trade.CreatedAt = &createdAt
		trade.UpdatedAt = &updatedAt
		trade.Signal = &model.AutochartistSignal{}
		if err := json.Unmarshal([]byte(rawData), trade.Signal); err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not unmarshal trade id: %s", tradeID)
		}

		//add trade to results
		results[trade.TradeID] = trade
	}

	return results, nil
}

//GetStrategyClosedTradesByTimeRange retrieves trades of a given strategy which were closed within a given time range
func GetStrategyClosedTradesByTimeRange(strategyName string, fromTime, toTime time.Time, db *sql.DB) (map[string]*model.SignalTrade, error) {
	if db == nil {
		db = conn
	}

	query := `
		SELECT trade_id, signal_id, strategy_name, trades.instrument, units, entry_price, entry_time, deadline, take_price,
		stop_price, ts_distance, exit_price, exit_time, profit_loss, trades.created_at, trades.updated_at, raw_data
		FROM signal_trades AS trades
			JOIN autochartist_signals AS signals ON trades.signal_id = signals.id
		WHERE trades.deleted_at IS NULL AND trades.strategy_name = ? AND trades.exit_time BETWEEN ? AND ?`

	rows, err := db.Query(query, strategyName, fromTime, toTime)
	if err != nil {
		return nil, fmt.Errorf("could not get signal trades >> %w", err)
	}

	results := make(map[string]*model.SignalTrade)
	defer rows.Close()

	for rows.Next() {
		if rows.Err() != nil {
			return nil, rows.Err()
		}

		//column vars
		var tradeID string
		var signalID int
		var strategyName string
		var instrument string
		var units int
		var entryPrice float64
		var entryTime time.Time
		var deadline sql.NullTime
		var takePrice sql.NullFloat64
		var stopPrice sql.NullFloat64
		var tsDistance sql.NullFloat64
		var exitPrice sql.NullFloat64
		var exitTime sql.NullTime
		var profitLoss sql.NullFloat64
		var createdAt time.Time
		var updatedAt time.Time
		var rawData string
		if err := rows.Scan(&tradeID, &signalID, &strategyName, &instrument, &units, &entryPrice, &entryTime, &deadline, &takePrice,
			&stopPrice, &tsDistance, &exitPrice, &exitTime, &profitLoss, &createdAt, &updatedAt, &rawData); err != nil {
			return nil, err
		}

		//build trade
		trade := &model.SignalTrade{}
		trade.TradeID = tradeID
		trade.StrategyName = strategyName
		trade.Instrument = instruments.ParseInstrument(instrument)
		trade.Units = units
		trade.EntryPrice = entryPrice
		trade.EntryTime = entryTime
		trade.EntryTimeRFC1123 = entryTime.Format(time.RFC1123)
		if deadline.Valid {
			trade.Deadline = &deadline.Time
			trade.DeadlineRFC1123 = trade.Deadline.Format(time.RFC1123)
		}
		if takePrice.Valid {
			trade.TakePrice = &takePrice.Float64
		}
		if stopPrice.Valid {
			trade.StopPrice = &stopPrice.Float64
		}
		if tsDistance.Valid {
			trade.TSDistance = &tsDistance.Float64
		}
		if exitPrice.Valid {
			trade.ExitPrice = &exitPrice.Float64
		}
		if exitTime.Valid {
			trade.ExitTime = &exitTime.Time
			trade.ExitTimeRFC1123 = trade.ExitTime.Format(time.RFC1123)
		}
		if profitLoss.Valid {
			trade.ProfitLoss = &profitLoss.Float64
		}
		trade.CreatedAt = &createdAt
		trade.UpdatedAt = &updatedAt
		trade.Signal = &model.AutochartistSignal{}
		if err := json.Unmarshal([]byte(rawData), trade.Signal); err != nil {
			ts := loggers.GetTimestamp()
			loggers.GetJSONLogger().WithField("ts", ts).WithError(err).Errorf("could not unmarshal trade id: %s", tradeID)
		}

		//add trade to results
		results[trade.TradeID] = trade
	}

	return results, nil
}

//UpdateSignalTrade updates certain signal trade values
func UpdateSignalTrade(trade *model.SignalTrade, db *sql.DB) error {
	if db == nil {
		db = conn
	}

	//build query
	query := strings.Builder{}
	query.WriteString("UPDATE signal_trades SET updated_at = current_timestamp()")
	if trade.Deadline != nil {
		query.WriteString(fmt.Sprintf(", deadline = '%s'", trade.Deadline.Format("2006-01-02 15:04:05")))
	}
	if trade.TakePrice != nil {
		query.WriteString(fmt.Sprintf(", take_price = %.6f", *trade.TakePrice))
	}
	if trade.StopPrice != nil {
		query.WriteString(fmt.Sprintf(", stop_price = %.6f", *trade.StopPrice))
	}
	if trade.TSDistance != nil {
		query.WriteString(fmt.Sprintf(", ts_distance = %.6f", *trade.TSDistance))
	}
	if trade.ExitPrice != nil {
		query.WriteString(fmt.Sprintf(", exit_price = %.6f", *trade.ExitPrice))
	}
	if trade.ExitTime != nil {
		query.WriteString(fmt.Sprintf(", exit_time = '%s'", trade.ExitTime.Format("2006-01-02 15:04:05")))
	}
	if trade.ProfitLoss != nil {
		query.WriteString(fmt.Sprintf(", profit_loss = %.2f", *trade.ProfitLoss))
	}
	query.WriteString(" WHERE trade_id = ?")

	//update trade
	_, err := db.Exec(query.String(), trade.TradeID)
	if err != nil {
		return fmt.Errorf("could not update trade >> %w", err)
	}

	return nil
}
