package db

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"net/url"
	"path/filepath"
	"time"

	"gofx/env"

	//mysql driver
	_ "github.com/go-sql-driver/mysql"
)

func init() {
	ErrUniqueFieldViolation = DuplicateValueError{}
}

//Conn is the pooled connection to the MySQL DB
var conn *sql.DB

//GetConnection returns connection to db
func GetConnection(schemaName *string) (*sql.DB, error) {
	var err error
	if conn == nil {
		schema := env.GetConfig().DBSchema
		if schemaName != nil {
			schema = *schemaName
		}
		dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s%s",
			env.GetConfig().DBUsername,
			env.GetConfig().DBPassword,
			env.GetConfig().DBHost,
			env.GetConfig().DBPort,
			schema,
			fmt.Sprintf("?columnsWithAlias=true&interpolateParams=true&multiStatements=true&parseTime=true&loc=%s", url.QueryEscape(time.Local.String())),
		)
		conn, err = sql.Open("mysql", dsn)
		if err != nil {
			conn = nil
		}
	}
	if err == nil {
		err = conn.Ping()
		if err != nil {
			conn = nil
		}
	}
	return conn, err
}

//CreateSchema creates a new db with name schemaName if not exists
func CreateSchema(schemaName *string) error {
	schema := env.GetConfig().DBSchema
	if schemaName != nil && len(*schemaName) > 0 {
		schema = *schemaName
	}
	if _, err := GetConnection(&schema); err != nil {
		//try to connect with no schema
		conn = nil
		schema = ""
		conn, err = GetConnection(&schema)
		if err != nil {
			return err
		}

		//create schema
		schema = env.GetConfig().DBSchema
		if schemaName != nil && len(*schemaName) > 0 {
			schema = *schemaName
		}
		_, err := conn.Exec(fmt.Sprintf("CREATE SCHEMA IF NOT EXISTS %s", schema))
		if err != nil {
			return err
		}

		//replace connection
		conn = nil
		if _, err := GetConnection(&schema); err != nil {
			if err != nil {
				return err
			}
		}
	}

	return nil
}

//ExecScript executes an sql script at the given file path
func ExecScript(filePathParts ...string) (sql.Result, error) {
	path := filepath.Join(filePathParts...)
	var script string

	if fileBytes, err := ioutil.ReadFile(path); err == nil {
		script = string(fileBytes)
	} else {
		return nil, err
	}

	//run script
	results, err := conn.Exec(script)
	return results, err
}
