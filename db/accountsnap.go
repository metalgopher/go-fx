package db

import (
	"database/sql"
	"fmt"
	"time"

	"gofx/model"
)

//CreateAccountSnapshot creates a new account snapshot in the db
func CreateAccountSnapshot(snapshot *model.AccountSnapshot, db *sql.DB) error {
	if db == nil {
		db = conn
	}

	//insert snapshot
	_, err := db.Exec(`INSERT INTO account_snapshots(window_open_at, balance, NAV, unrealized_pl, margin_used, margin_available, 
		withdrawal_limit, open_trade_count, pending_order_count, financing, last_transaction_id, account_id, alias, currency, margin_rate, 
		created_by_user_id, raw_data, created_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
		snapshot.WindowOpenAt, snapshot.Balance, snapshot.NAV, snapshot.UnrealizedPL, snapshot.MarginUsed, snapshot.MarginAvailable,
		snapshot.WithdrawalLimit, snapshot.OpenTradeCount, snapshot.PendingOrderCount, snapshot.Financing, snapshot.LastTransactionID,
		snapshot.AccountID, snapshot.Alias, snapshot.Currency, snapshot.MarginRate, snapshot.CreatedByUserID, snapshot.AcctRawData,
		snapshot.CreatedAt,
	)
	if err != nil {
		return fmt.Errorf("could not create new account snapshot >> %w", err)
	}

	return nil
}

//GetAccountSnapshotByWindowOpen retrieves an account snapshot by its window open time
func GetAccountSnapshotByWindowOpen(windowOpen time.Time, db *sql.DB) (*model.AccountSnapshot, error) {
	if db == nil {
		db = conn
	}

	query := `
		SELECT id, window_open_at, balance, NAV, unrealized_pl, margin_used, margin_available, 
			withdrawal_limit, open_trade_count, pending_order_count, financing, last_transaction_id, account_id, 
			alias, currency, margin_rate, created_by_user_id, raw_data, created_at, updated_at
		FROM account_snapshots
		WHERE deleted_at IS NULL AND window_open_at = ?`

	result := db.QueryRow(query, windowOpen)

	//column vars
	var id int
	var windowOpenAt time.Time
	var balance float64
	var NAV float64
	var unrealizedPL float64
	var marginUsed float64
	var marginAvail float64
	var withdrawalLimit float64
	var openTradeCount int
	var pendingOrderCount int
	var financing float64
	var lastTransactionID sql.NullString
	var accountID string
	var alias string
	var currency string
	var marginRate float64
	var createdByUserID int
	var rawData string
	var createdAt time.Time
	var updatedAt time.Time
	if err := result.Scan(&id, &windowOpenAt, &balance, &NAV, &unrealizedPL, &marginUsed, &marginAvail, &withdrawalLimit, &openTradeCount, &pendingOrderCount,
		&financing, &lastTransactionID, &accountID, &alias, &currency, &marginRate, &createdByUserID, &rawData, &createdAt, &updatedAt); err != nil {
		return nil, err
	}

	//build snapshot result
	snapshot := &model.AccountSnapshot{}
	snapshot.WindowOpenAt = windowOpenAt
	snapshot.WindowOpenAtRFC1123 = windowOpenAt.Format(time.RFC1123)
	snapshot.Balance = balance
	snapshot.NAV = NAV
	snapshot.UnrealizedPL = unrealizedPL
	snapshot.MarginUsed = marginUsed
	snapshot.MarginAvailable = marginAvail
	snapshot.WithdrawalLimit = withdrawalLimit
	snapshot.OpenTradeCount = openTradeCount
	snapshot.PendingOrderCount = pendingOrderCount
	snapshot.Financing = financing
	if lastTransactionID.Valid {
		snapshot.LastTransactionID = &lastTransactionID.String
	}
	snapshot.AccountID = accountID
	snapshot.Alias = alias
	snapshot.Currency = currency
	snapshot.MarginRate = marginRate
	snapshot.CreatedByUserID = createdByUserID
	snapshot.AcctRawData = rawData
	snapshot.CreatedAt = &createdAt
	snapshot.UpdatedAt = &updatedAt

	return snapshot, nil
}
