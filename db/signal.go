package db

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"time"

	"gofx/model"
)

//CreateSignal creates a new signal in the db
func CreateSignal(signal *model.AutochartistSignal, db *sql.DB) error {
	if db == nil {
		db = conn
	}

	//marshal full signal to JSON
	rawJSON, _ := json.Marshal(signal)

	//optional prediction values
	var priceLow, priceHigh *float64
	var timeFrom, timeTo *time.Time
	if signal.Data.Prediction != nil {
		priceLow = signal.Data.Prediction.PriceLow
		priceHigh = signal.Data.Prediction.PriceHigh
		timeFrom = signal.Data.Prediction.TimeFrom
		timeTo = signal.Data.Prediction.TimeTo
	}

	//insert signal
	_, err := db.Exec(`INSERT INTO autochartist_signals(id, signal_type, instrument, completed, probability, signal_interval, 
		direction, pattern, price_low, price_high, time_from, time_to, raw_data) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
		signal.ID, signal.Type, signal.Instrument.Name(), signal.Meta.Completed, signal.Meta.Probability, signal.Meta.Interval,
		signal.Meta.Direction, signal.Meta.Pattern, priceLow, priceHigh, timeFrom, timeTo, string(rawJSON))
	if err != nil {
		return fmt.Errorf("could not create new signal >> %w", err)
	}

	return nil
}
